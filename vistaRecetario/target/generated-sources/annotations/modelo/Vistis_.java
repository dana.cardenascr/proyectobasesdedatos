package modelo;

import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Recipes;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(Vistis.class)
public class Vistis_ { 

    public static volatile SingularAttribute<Vistis, Date> date;
    public static volatile SingularAttribute<Vistis, Integer> usersId1;
    public static volatile SingularAttribute<Vistis, Integer> id;
    public static volatile SingularAttribute<Vistis, Recipes> recipesId;

}
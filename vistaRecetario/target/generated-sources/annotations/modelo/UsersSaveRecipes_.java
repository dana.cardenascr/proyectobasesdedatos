package modelo;

import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Recipes;
import modelo.Users;
import modelo.UsersSaveRecipesPK;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(UsersSaveRecipes.class)
public class UsersSaveRecipes_ { 

    public static volatile SingularAttribute<UsersSaveRecipes, Date> date;
    public static volatile SingularAttribute<UsersSaveRecipes, Recipes> recipes;
    public static volatile SingularAttribute<UsersSaveRecipes, UsersSaveRecipesPK> usersSaveRecipesPK;
    public static volatile SingularAttribute<UsersSaveRecipes, Users> users;

}
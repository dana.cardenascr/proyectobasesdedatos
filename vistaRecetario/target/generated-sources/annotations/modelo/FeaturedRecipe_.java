package modelo;

import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.FeaturedRecipePK;
import modelo.Recipes;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(FeaturedRecipe.class)
public class FeaturedRecipe_ { 

    public static volatile SingularAttribute<FeaturedRecipe, FeaturedRecipePK> featuredRecipePK;
    public static volatile SingularAttribute<FeaturedRecipe, Recipes> recipes;
    public static volatile SingularAttribute<FeaturedRecipe, Date> endDate;
    public static volatile SingularAttribute<FeaturedRecipe, Date> startDate;

}
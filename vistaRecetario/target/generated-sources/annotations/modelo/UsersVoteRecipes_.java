package modelo;

import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Recipes;
import modelo.Users;
import modelo.UsersVoteRecipesPK;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(UsersVoteRecipes.class)
public class UsersVoteRecipes_ { 

    public static volatile SingularAttribute<UsersVoteRecipes, Date> date;
    public static volatile SingularAttribute<UsersVoteRecipes, Recipes> recipes;
    public static volatile SingularAttribute<UsersVoteRecipes, UsersVoteRecipesPK> usersVoteRecipesPK;
    public static volatile SingularAttribute<UsersVoteRecipes, Users> users;

}
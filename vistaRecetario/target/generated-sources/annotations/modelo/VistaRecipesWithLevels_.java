package modelo;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(VistaRecipesWithLevels.class)
public class VistaRecipesWithLevels_ { 

    public static volatile SingularAttribute<VistaRecipesWithLevels, String> image;
    public static volatile SingularAttribute<VistaRecipesWithLevels, String> level;
    public static volatile SingularAttribute<VistaRecipesWithLevels, Float> totalTime;
    public static volatile SingularAttribute<VistaRecipesWithLevels, String> name;
    public static volatile SingularAttribute<VistaRecipesWithLevels, Float> preparationTime;
    public static volatile SingularAttribute<VistaRecipesWithLevels, String> description;
    public static volatile SingularAttribute<VistaRecipesWithLevels, Integer> id;
    public static volatile SingularAttribute<VistaRecipesWithLevels, String> preparationInstructions;
    public static volatile SingularAttribute<VistaRecipesWithLevels, Float> cookingTime;
    public static volatile SingularAttribute<VistaRecipesWithLevels, Integer> portions;

}
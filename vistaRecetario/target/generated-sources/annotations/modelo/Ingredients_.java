package modelo;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.RecipesHasIngredients;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(Ingredients.class)
public class Ingredients_ { 

    public static volatile ListAttribute<Ingredients, RecipesHasIngredients> recipesHasIngredientsList;
    public static volatile SingularAttribute<Ingredients, String> name;
    public static volatile SingularAttribute<Ingredients, String> description;
    public static volatile SingularAttribute<Ingredients, Integer> id;

}
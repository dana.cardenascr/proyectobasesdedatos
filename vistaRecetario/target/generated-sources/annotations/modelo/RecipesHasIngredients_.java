package modelo;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Ingredients;
import modelo.MeasurementUnits;
import modelo.Recipes;
import modelo.RecipesHasIngredientsPK;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(RecipesHasIngredients.class)
public class RecipesHasIngredients_ { 

    public static volatile SingularAttribute<RecipesHasIngredients, Recipes> recipes;
    public static volatile SingularAttribute<RecipesHasIngredients, Float> amount;
    public static volatile SingularAttribute<RecipesHasIngredients, RecipesHasIngredientsPK> recipesHasIngredientsPK;
    public static volatile SingularAttribute<RecipesHasIngredients, Ingredients> ingredients;
    public static volatile SingularAttribute<RecipesHasIngredients, MeasurementUnits> measurementUnits;

}
package modelo;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Recipes;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(Occasions.class)
public class Occasions_ { 

    public static volatile SingularAttribute<Occasions, String> occasion;
    public static volatile SingularAttribute<Occasions, Integer> id;
    public static volatile ListAttribute<Occasions, Recipes> recipesList;

}
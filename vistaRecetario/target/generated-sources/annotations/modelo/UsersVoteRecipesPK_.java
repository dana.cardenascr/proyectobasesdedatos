package modelo;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(UsersVoteRecipesPK.class)
public class UsersVoteRecipesPK_ { 

    public static volatile SingularAttribute<UsersVoteRecipesPK, Integer> usersId;
    public static volatile SingularAttribute<UsersVoteRecipesPK, Integer> recipesId;

}
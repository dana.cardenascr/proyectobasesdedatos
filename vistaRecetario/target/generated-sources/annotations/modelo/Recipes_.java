package modelo;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Categories;
import modelo.FeaturedRecipe;
import modelo.Levels;
import modelo.Occasions;
import modelo.Recipes;
import modelo.RecipesHasIngredients;
import modelo.UsersSaveRecipes;
import modelo.UsersVoteRecipes;
import modelo.Vistis;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-07-04T18:58:27", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(Recipes.class)
public class Recipes_ { 

    public static volatile SingularAttribute<Recipes, String> image;
    public static volatile ListAttribute<Recipes, UsersSaveRecipes> usersSaveRecipesList;
    public static volatile SingularAttribute<Recipes, Float> totalTime;
    public static volatile SingularAttribute<Recipes, Float> preparationTime;
    public static volatile ListAttribute<Recipes, Occasions> occasionsList;
    public static volatile ListAttribute<Recipes, FeaturedRecipe> featuredRecipeList;
    public static volatile SingularAttribute<Recipes, String> description;
    public static volatile ListAttribute<Recipes, Recipes> recipesList1;
    public static volatile ListAttribute<Recipes, Recipes> recipesList;
    public static volatile SingularAttribute<Recipes, Float> cookingTime;
    public static volatile ListAttribute<Recipes, RecipesHasIngredients> recipesHasIngredientsList;
    public static volatile ListAttribute<Recipes, Vistis> vistisList;
    public static volatile SingularAttribute<Recipes, String> name;
    public static volatile ListAttribute<Recipes, Categories> categoriesList;
    public static volatile SingularAttribute<Recipes, Levels> levelsId;
    public static volatile ListAttribute<Recipes, UsersVoteRecipes> usersVoteRecipesList;
    public static volatile SingularAttribute<Recipes, Integer> id;
    public static volatile SingularAttribute<Recipes, String> preparationInstructions;
    public static volatile SingularAttribute<Recipes, Integer> portions;

}
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package vista;

import controlador.ControladorFrmRecetas;
import java.awt.Color;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.text.DefaultCaret;

/**
 *
 * @author Ashley
 */
public class JpnlDatosRecetas extends javax.swing.JPanel {

    
    private String path = "";
    /**
     * Creates new form jpnlDatos
     */
    public JpnlDatosRecetas() {
        initComponents();
        setTransparencia();
    }//Fin constructor

    public void escuchar(ActionListener controladorFrmRecetas) {
        btnBuscar.addActionListener(controladorFrmRecetas);
        btnImagen.addActionListener(controladorFrmRecetas);
    }//Fin escuchar

    public void setTransparencia() {
        cbxComplejidad.setBackground(new Color(75, 92, 113));
        scrollPane.getViewport().setOpaque(false);
        scrollPane.setBackground(new Color(0, 0, 0, 0));
        txtInstrucciones.setOpaque(false);
        txtInstrucciones.setBackground(new Color(0, 0, 0, 0));
        scrollPane1.getViewport().setOpaque(false);
        scrollPane1.setBackground(new Color(0, 0, 0, 0));
        txtDescripcion.setOpaque(false);
        txtDescripcion.setBackground(new Color(0, 0, 0, 0));
        txtNombreReceta.setOpaque(false);
        txtNombreReceta.setBackground(new Color(0, 0, 0, 0));
        txtId.setOpaque(false);
        txtId.setBackground(new Color(0, 0, 0, 0));
        txtPorciones.setOpaque(false);
        txtPorciones.setBackground(new Color(0, 0, 0, 0));
        txtTiempoCoccion.setOpaque(false);
        txtTiempoCoccion.setBackground(new Color(0, 0, 0, 0));
        txtTiempoPreparacion.setOpaque(false);
        txtTiempoPreparacion.setBackground(new Color(0, 0, 0, 0));
        txtTiempoTotal.setOpaque(false);
        txtTiempoTotal.setBackground(new Color(0, 0, 0, 0));
    }//Fin setTransparencia

    public void setTxtNombreReceta(String nombreReceta) {
        this.txtNombreReceta.setText(nombreReceta);
    }//Fin setTxtNombreReceta

    public String getTxtNombreReceta() {
        if (txtNombreReceta.getText().equals("")) {
            return "Nada";
        }//Fin if
        else {
            return txtNombreReceta.getText();
        }//Fin else
    }//Fin getTxtNombreReceta

    public int getTxtId() {
        if (txtId.getText().equals("")) {
            return -1;
        }//Fin if
        else {
            return Integer.parseInt(txtId.getText());
        }//Fin else
    }//Fin getTxtId

    public void setTxtId(int id) {
        this.txtId.setText(Integer.toString(id));
    }//Fin setTxtId

    public int getTxtPorciones() {
        if (txtPorciones.getText().equals("")) {
            return -1;
        }//Fin if
        else {
            return Integer.parseInt(txtPorciones.getText());
        }//Fin else
    }//Fin getTxtPorciones

    public void setTxtPorciones(int id) {
        this.txtPorciones.setText(Integer.toString(id));
    }//Fin setTxtPorciones

    public float getTxtTiempoCoccion() {
        if (txtTiempoCoccion.getText().equals("")) {
            return -1.0f;
        } else {
            return Float.parseFloat(txtTiempoCoccion.getText());
        }//Fin else
    }//Fin getTxtTiempoCoccion

    public void setTxtTiempoCoccion(float tiempo) {
        this.txtTiempoCoccion.setText(Float.toString(tiempo));
    }//Fin setTxtTiempoCoccion

    public float getTxtTiempoPreparacion() {
        if (txtTiempoPreparacion.getText().equals("")) {
            return -1.0f;
        } else {
            return Float.parseFloat(txtTiempoPreparacion.getText());
        }//Fin else
    }//Fin getTxtTiempoPreparacion

    public void setTxtTiempoPreparacion(float tiempoPreparacion) {
        this.txtTiempoPreparacion.setText(Float.toString(tiempoPreparacion));
    }//Fin setTxtTiempoPreparacion

    public float getTxtTiempoTotal() {
        if (txtTiempoTotal.getText().equals("")) {
            return -1.0f;
        } else {
            return Float.parseFloat(txtTiempoTotal.getText());
        }//Fin else
    }//Fin getTxtTiempoTotal

    public void setTxtTiempoTotal(float tiempoTotal) {
        this.txtTiempoTotal.setText(Float.toString(tiempoTotal));
    }//Fin setTxtTiempoTotal

    public void setCategoria(String categoria) {
        cbxComplejidad.setSelectedItem(categoria);
    }//Fin setCategoria

    public String getCategoria() {
        return cbxComplejidad.getSelectedItem().toString();
    }//Fin getCategoria

    public void setComplejidad(String complejidad) {
        cbxComplejidad.setSelectedItem(complejidad);
    }//Fin setComplejidad

    public String getComplejidad() {
        return cbxComplejidad.getSelectedItem().toString();
    }//Fin getComplejidad

    public int getIndiceSeleccionado(String tipo) {
        switch (tipo) {
            case "complejidad":
                return cbxComplejidad.getSelectedIndex();
        }//Fin switch
        return -1;
    }//Fin IndiceSeleccionado

    public String getTxtInstrucciones() {
        if (txtInstrucciones.getText().equals("")) {
            return "Nada";
        }//Fin if
        else {
            return txtInstrucciones.getText();
        }//Fin else
    }//Fin getTxtInstrucciones

    public void setTxtInstrucciones(String instrucciones) {
        this.txtInstrucciones.setText(instrucciones);
    }//Fin setTxtInstrucciones

    public String getTxtDescripcion() {
        if (txtDescripcion.getText().equals("")) {
            return "Nada";
        }//Fin if
        else {
            return txtDescripcion.getText();
        }//Fin else
    }//Fin getTxtDescripcion

    public void setTxtDescripcion(String descripcion) {
        this.txtDescripcion.setText(descripcion);
    }//Fin setTxtDescripcion

    public void limpiar() {
        this.txtNombreReceta.setText("");
        this.txtId.setText("");
        this.txtTiempoCoccion.setText("");
        this.txtTiempoPreparacion.setText("");
        this.txtTiempoTotal.setText("");
        this.cbxComplejidad.setSelectedIndex(0);
        this.cbxComplejidad.setSelectedIndex(0);
        this.txtInstrucciones.setText("");
        this.txtDescripcion.setText("");
        this.txtPorciones.setText("");
        setImagen("");
    }//Fin limpiar
    
    public void setImagen(String path){
        this.path = path;
        jlImagen.setIcon(new ImageIcon(path));
    }//Fin setImagen
    
    public String getImagePath(){
        return path;
    }//Fin getImagen

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cbxComplejidad = new javax.swing.JComboBox<>();
        scrollPane1 = new javax.swing.JScrollPane();
        txtDescripcion = new javax.swing.JTextArea();
        txtPorciones = new javax.swing.JTextField();
        scrollPane = new javax.swing.JScrollPane();
        txtInstrucciones = new javax.swing.JTextArea();
        btnImagen = new javax.swing.JButton();
        jlImagen = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        txtNombreReceta = new javax.swing.JTextField();
        txtId = new javax.swing.JTextField();
        txtTiempoCoccion = new javax.swing.JTextField();
        txtTiempoPreparacion = new javax.swing.JTextField();
        txtTiempoTotal = new javax.swing.JTextField();
        jlFondo = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(1366, 768));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cbxComplejidad.setForeground(new java.awt.Color(255, 255, 255));
        cbxComplejidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select category", "Easy", "Intermediate", "Advanced" }));
        add(cbxComplejidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 307, 250, 40));

        scrollPane1.setBorder(null);
        scrollPane1.setOpaque(false);

        txtDescripcion.setColumns(20);
        txtDescripcion.setForeground(new java.awt.Color(255, 255, 255));
        txtDescripcion.setRows(5);
        txtDescripcion.setBorder(null);
        txtDescripcion.setOpaque(false);
        scrollPane1.setViewportView(txtDescripcion);

        add(scrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 410, 230, 170));

        txtPorciones.setForeground(new java.awt.Color(255, 255, 255));
        txtPorciones.setBorder(null);
        add(txtPorciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 115, 90, 30));

        scrollPane.setBorder(null);
        scrollPane.setOpaque(false);

        txtInstrucciones.setColumns(20);
        txtInstrucciones.setForeground(new java.awt.Color(255, 255, 255));
        txtInstrucciones.setRows(5);
        txtInstrucciones.setBorder(null);
        txtInstrucciones.setOpaque(false);
        scrollPane.setViewportView(txtInstrucciones);

        add(scrollPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 412, 230, 170));

        btnImagen.setActionCommand("agregarImagen");
        btnImagen.setBorderPainted(false);
        btnImagen.setContentAreaFilled(false);
        btnImagen.setDefaultCapable(false);
        add(btnImagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 283, 130, 120));
        add(jlImagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 129, 410, 430));

        btnBuscar.setActionCommand("buscar");
        btnBuscar.setBorderPainted(false);
        btnBuscar.setContentAreaFilled(false);
        btnBuscar.setDefaultCapable(false);
        add(btnBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 620, 200, 60));

        txtNombreReceta.setForeground(new java.awt.Color(255, 255, 255));
        txtNombreReceta.setBorder(null);
        add(txtNombreReceta, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 115, 230, 30));

        txtId.setForeground(new java.awt.Color(255, 255, 255));
        txtId.setBorder(null);
        add(txtId, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 115, 90, 30));

        txtTiempoCoccion.setForeground(new java.awt.Color(255, 255, 255));
        txtTiempoCoccion.setBorder(null);
        add(txtTiempoCoccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 217, 230, 30));

        txtTiempoPreparacion.setForeground(new java.awt.Color(255, 255, 255));
        txtTiempoPreparacion.setBorder(null);
        add(txtTiempoPreparacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 217, 230, 30));

        txtTiempoTotal.setForeground(new java.awt.Color(255, 255, 255));
        txtTiempoTotal.setBorder(null);
        add(txtTiempoTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 310, 230, 30));

        jlFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/RecetasV2.png"))); // NOI18N
        add(jlFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnImagen;
    private javax.swing.JComboBox<String> cbxComplejidad;
    private javax.swing.JLabel jlFondo;
    private javax.swing.JLabel jlImagen;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JScrollPane scrollPane1;
    private javax.swing.JTextArea txtDescripcion;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextArea txtInstrucciones;
    private javax.swing.JTextField txtNombreReceta;
    private javax.swing.JTextField txtPorciones;
    private javax.swing.JTextField txtTiempoCoccion;
    private javax.swing.JTextField txtTiempoPreparacion;
    private javax.swing.JTextField txtTiempoTotal;
    // End of variables declaration//GEN-END:variables
}//Fin clase

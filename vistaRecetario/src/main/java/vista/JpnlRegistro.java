/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package vista;

import java.awt.Color;
import java.awt.event.ActionListener;
import javax.swing.JTextField;

/**
 *
 * @author Ashley
 */
public class JpnlRegistro extends javax.swing.JPanel {

    /**
     * Creates new form JpnlRegistro
     */
    public JpnlRegistro() {
        initComponents();
        setTransparencia();
    }//Fin constructor

    public void escuchar(ActionListener controladorFrmRegistro) {
        btnRegistrarse.addActionListener(controladorFrmRegistro);
    }//Fin escuchar

    public void setTransparencia() {
        txtNombre.setOpaque(false);
        txtNombre.setBackground(new Color(0, 0, 0, 0));
        txtApellidos.setOpaque(false);
        txtApellidos.setBackground(new Color(0, 0, 0, 0));
        txtUsuario.setOpaque(false);
        txtUsuario.setBackground(new Color(0, 0, 0, 0));
        txtPais.setOpaque(false);
        txtPais.setBackground(new Color(0, 0, 0, 0));
        txtCorreo.setOpaque(false);
        txtCorreo.setBackground(new Color(0, 0, 0, 0));
        txtContrasena.setOpaque(false);
        txtContrasena.setBackground(new Color(0, 0, 0, 0));
    }//Fin setTransparencia

    public void setTxtNombre(String nombre) {
        this.txtNombre.setText(nombre);
    }//Fin setTxtNombre

    public String getTxtNombre() {
        if (txtNombre.getText().equals("")) {
            return "Nada";
        }//Fin if
        else {
            return txtNombre.getText();
        }//Fin else
    }//Fin getTxtNombre

    public void setTxtNombreApellidos(String apellidos) {
        this.txtNombre.setText(apellidos);
    }//Fin setTxtNombreApellidos

    public String getTxtApellidos() {
        if (txtApellidos.getText().equals("")) {
            return "Nada";
        }//Fin if
        else {
            return txtApellidos.getText();
        }//Fin else
    }//Fin getTxtApellidos

    public void setTxtApellidos(String nombreReceta) {
        this.txtApellidos.setText(nombreReceta);
    }//Fin setTxtNombreReceta

    public String getTxtCorreo() {
        if (txtCorreo.getText().equals("")) {
            return "Nada";
        }//Fin if
        else {
            return txtCorreo.getText();
        }//Fin else
    }//Fin getTxApellidos

    public void setTxtUsuario(String usuario) {
        this.txtUsuario.setText(usuario);
    }//Fin setTxtUsuario

    public String getTxtUsuario() {
        if (txtUsuario.getText().equals("")) {
            return "Nada";
        }//Fin if
        else {
            return txtUsuario.getText();
        }//Fin else
    }//Fin getTxtUsuario

    public void setTxtPais(String pais) {
        this.txtPais.setText(pais);
    }//Fin setTxtPais

    public String getTxtPais() {
        if (txtPais.getText().equals("")) {
            return "Nada";
        }//Fin if
        else {
            return txtPais.getText();
        }//Fin else
    }//Fin getTxtPais

    public void setTxtContrasena(String pais) {
        this.txtContrasena.setText(pais);
    }//Fin setTxtContrasena

    public String getTxtContrasena() {
        if (txtContrasena.getText().equals("")) {
            return "Nada";
        }//Fin if
        else {
            return txtContrasena.getText();
        }//Fin else
    }//Fin getTxtContrasena


    public void setTxtCorreo(String txtCorreo) {
        this.txtCorreo.setText(txtCorreo);
    }//Fin setTxtCorreo
    
    public void limpiar(){
        txtNombre.setText("");
        txtApellidos.setText("");
        txtUsuario.setText("");
        txtPais.setText("");
        txtCorreo.setText("");
        txtContrasena.setText("");
    }//Fin limpiar

    
    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRegistrarse = new javax.swing.JButton();
        txtNombre = new javax.swing.JTextField();
        txtApellidos = new javax.swing.JTextField();
        txtUsuario = new javax.swing.JTextField();
        txtPais = new javax.swing.JTextField();
        txtCorreo = new javax.swing.JTextField();
        txtContrasena = new javax.swing.JTextField();
        jlFondo = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnRegistrarse.setActionCommand("registro");
        btnRegistrarse.setBorderPainted(false);
        btnRegistrarse.setContentAreaFilled(false);
        add(btnRegistrarse, new org.netbeans.lib.awtextra.AbsoluteConstraints(575, 620, 220, 70));

        txtNombre.setForeground(new java.awt.Color(255, 255, 255));
        txtNombre.setBorder(null);
        add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 200, 220, 40));

        txtApellidos.setForeground(new java.awt.Color(255, 255, 255));
        txtApellidos.setBorder(null);
        add(txtApellidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(723, 200, 210, 40));

        txtUsuario.setForeground(new java.awt.Color(255, 255, 255));
        txtUsuario.setBorder(null);
        add(txtUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 320, 220, 30));

        txtPais.setForeground(new java.awt.Color(255, 255, 255));
        txtPais.setBorder(null);
        add(txtPais, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 320, 210, 30));

        txtCorreo.setForeground(new java.awt.Color(255, 255, 255));
        txtCorreo.setBorder(null);
        add(txtCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 430, 490, 40));

        txtContrasena.setForeground(new java.awt.Color(255, 255, 255));
        txtContrasena.setBorder(null);
        add(txtContrasena, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 550, 490, 30));

        jlFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Registro.png"))); // NOI18N
        add(jlFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRegistrarse;
    private javax.swing.JLabel jlFondo;
    private javax.swing.JTextField txtApellidos;
    private javax.swing.JTextField txtContrasena;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPais;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}//Fin clase

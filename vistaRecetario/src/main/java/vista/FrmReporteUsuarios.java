/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package vista;

import controlador.ControladorInicio;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author kmore
 */
public class FrmReporteUsuarios extends javax.swing.JFrame {

    /**
     * Creates new form FrmReporteUsuarios
     */
    public FrmReporteUsuarios() {
        initComponents();
     setLocationRelativeTo(null);
    }

     public void escuchar(ActionListener controladorInicio){
        jButtonVolver.addActionListener(controladorInicio);
    }
    
    public void setTabla(String [] etiquetas, String [][] datos){
        this.jTListaUsuarios.setModel(new DefaultTableModel(datos,etiquetas));
        jScrollPane1.setViewportView(jTListaUsuarios);
    }
    
    public String[] getFilaTabla(){
        String[] filaDatos=new String[this.jTListaUsuarios.getColumnCount()];
        int filaNum=this.jTListaUsuarios.getSelectedRow();
        for (int i = 0; i < filaDatos.length; i++) {
            filaDatos[i]=this.jTListaUsuarios.getModel().getValueAt(filaNum, i).toString();
        }
        return filaDatos;
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonVolver = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTListaUsuarios = new javax.swing.JTable();
        jlFondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButtonVolver.setActionCommand("Volver1");
        jButtonVolver.setBorderPainted(false);
        jButtonVolver.setContentAreaFilled(false);
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 600, 60, 50));

        jTListaUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTListaUsuarios);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 160, -1, -1));

        jlFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ListaUsuarios.jpeg"))); // NOI18N
        getContentPane().add(jlFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1159, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonVolverActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTListaUsuarios;
    private javax.swing.JLabel jlFondo;
    // End of variables declaration//GEN-END:variables
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.ArrayUsers;
import modelo.Users;
import vista.FrmInicio;
import vista.FrmInicioSesion;
import vista.FrmListaRecetas;
import vista.FrmRecetas;
import vista.FrmRegistro;
import vista.JpnlInicioSesion;

/**
 *
 * @author Ashley
 */
public class ControladorFrmInicioSesion implements ActionListener {

    private FrmInicio frmInicio;
    private FrmInicioSesion frmInicioSesion;
    private FrmListaRecetas frmLista;
    private FrmRegistro frmRegistro;
    private FrmRecetas frmRecetas;
    private JpnlInicioSesion panel;
    private ArrayUsers arrayUser;
    private ControladorInicio controladorInicio;
    private ControladorFrmLista ctrLista;
    private ControladorFrmRecetas ctrRecetas;
    private ControladorFrmRegistro ctrRegistro;
    private boolean privilegedAccess;
    private boolean access;

    public ControladorFrmInicioSesion(FrmInicio frmInicio, FrmInicioSesion frmInicioSesion, FrmListaRecetas frmLista, FrmRegistro frmRegistro, FrmRecetas frmRecetas, ArrayUsers arrayUser) {
        this.frmInicio = frmInicio;
        this.frmInicioSesion = frmInicioSesion;
        this.frmLista = frmLista;
        this.frmRegistro = frmRegistro;
        this.frmRecetas = frmRecetas;
        this.arrayUser = arrayUser;
        panel = frmInicioSesion.getPanel();
        privilegedAccess = false;
    }//Fin constructor

    public ControladorFrmInicioSesion(FrmInicio frmInicio, FrmInicioSesion frmInicioSesion, FrmListaRecetas frmLista, FrmRegistro frmRegistro, FrmRecetas frmRecetas,ArrayUsers arrayUser, ControladorInicio controladorInicio, ControladorFrmLista ctrLista, ControladorFrmRecetas ctrRecetas, ControladorFrmRegistro ctrRegistro) {
        this.frmInicio = frmInicio;
        this.frmInicioSesion = frmInicioSesion;
        this.frmLista = frmLista;
        this.frmRegistro = frmRegistro;
        this.frmRecetas = frmRecetas;
        this.panel = panel;
        this.arrayUser = arrayUser;
        this.controladorInicio = controladorInicio;
        this.ctrLista = ctrLista;
        this.ctrRecetas = ctrRecetas;
        this.ctrRegistro = ctrRegistro;
        panel = frmInicioSesion.getPanel();
        privilegedAccess = false;
    }//Fin constructor

    public boolean isDatosIncompletos() {
        if ((panel.getTxtUsuario().equals("Nada")) || (panel.getTxContrasena().equals("Nada"))) {
            return true;
        }//Fin if
        return false;
    }//Fin isDatosIncompletos

    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "inicioSesion":
                if (isDatosIncompletos()) {
                    frmInicioSesion.mostrarMensaje("Complete all the data before proceeding");
                }//Fin if
                else {
                    if (arrayUser.buscarUserName(panel.getTxtUsuario()) != null) {
                        Users user = arrayUser.buscarUserName(panel.getTxtUsuario());
                        if (user.getPassword().equals(panel.getTxContrasena())) {
                            frmInicioSesion.mostrarMensaje("Welcome back, " + panel.getTxtUsuario()+"!");
                            privilegedAccess = true;
                            controladorInicio.setPrivilegedAccess(privilegedAccess);
                            ctrLista.setPrivilegedAccess(privilegedAccess);
                            ctrRecetas.setPrivilegedAccess(privilegedAccess);
                            ctrRegistro.setPrivilegedAccess(privilegedAccess);
                            frmInicioSesion.setVisible(false);
                            frmInicio.setVisible(true);
                            panel.limpiar();
                        } else {
                            frmInicioSesion.mostrarMensaje("Incorrect password");
                        }//Fin else
                    } else {
                        frmInicioSesion.mostrarMensaje("Incorrect username");
                    }//Fin else
                }//Fin else
                break;

            case "invitado":
                frmInicioSesion.mostrarMensaje("Welcome, you have logged in as a guest");
                privilegedAccess = false;
                controladorInicio.setPrivilegedAccess(privilegedAccess);
                ctrLista.setPrivilegedAccess(privilegedAccess);
                ctrRecetas.setPrivilegedAccess(privilegedAccess);
                ctrRegistro.setPrivilegedAccess(privilegedAccess);
                access = true;
                controladorInicio.setAccess(access);
                ctrLista.setAccess(access);
                ctrRecetas.setAccess(access);
                ctrRegistro.setAccess(access);
                frmInicioSesion.setVisible(false);
                frmInicio.setVisible(true);
                break;

            case "iniciarSesion":
                System.out.println("Apretaste para ir a la ventana para iniciar sesión");
                frmInicioSesion.setVisible(false);
                break;

            case "registrarse":
                System.out.println("Apretaste para ir a la ventana para registrarse");
                frmInicioSesion.setVisible(false);
                frmRegistro.setVisible(true);
                break;

            case "lista":
                if (privilegedAccess || access) {
                    System.out.println("Apretaste para ir a la ventana para ver la lista");
                    frmInicioSesion.setVisible(false);
                    frmLista.setVisible(true);
                } else {
                    frmInicioSesion.mostrarMensaje("First you have to log in to use this option");
                }
                break;

            case "manipularReceta":
                if (privilegedAccess) {
                    System.out.println("Apretaste para ir a la ventana para manipular recetas");
                    frmInicioSesion.setVisible(false);
                    frmRecetas.setVisible(true);
                } else {
                    frmInicioSesion.mostrarMensaje("Only users can access this option");
                }
                break;
        }//Fin switch
    }//Fin actionPerformed

}//Fin clase

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.ArrayRecipes;
import modelo.ArrayUsers;
import modelo.Recipes;
import modelo.Users;
import vista.FrmInicio;
import vista.FrmInicioSesion;
import vista.FrmListaRecetas;
import vista.FrmRecetas;
import vista.FrmRegistro;
import vista.FrmReporteRecetas;
import vista.FrmReporteUsuarios;
import vista.JpnlListaRecetas;

/**
 *
 * @author Ashley
 */
public class ControladorFrmLista implements ActionListener {

    private FrmInicio frmInicio;
    private FrmInicioSesion frmInicioSesion;
    private FrmListaRecetas frmLista;
    private FrmRegistro frmRegistro;
    private FrmRecetas frmRecetas;
    private JpnlListaRecetas panel;
    private ArrayRecipes arrayRecipes;
    private ArrayUsers arrayUsers;
    private FrmReporteUsuarios frmTablaUsuarios;
    private FrmReporteRecetas frmTablaRecetas;
    private boolean privilegedAccess;
    private boolean access;
    int receta1 = 0, receta2 = 1, receta3 = 2;

    public ControladorFrmLista(FrmInicio frmInicio, FrmInicioSesion frmInicioSesion, FrmListaRecetas frmLista, FrmRegistro frmRegistro, FrmRecetas frmRecetas, ArrayRecipes arrayRecipes) {
        this.frmInicio = frmInicio;
        this.frmInicioSesion = frmInicioSesion;
        this.frmLista = frmLista;
        this.frmRegistro = frmRegistro;
        this.frmRecetas = frmRecetas;
        this.arrayRecipes = arrayRecipes;
        panel = frmLista.getPanel();
        actualizarRecetas();
    }//Fin constructor

    public ControladorFrmLista(FrmInicio frmInicio, FrmInicioSesion frmInicioSesion, FrmListaRecetas frmLista, FrmRegistro frmRegistro, FrmRecetas frmRecetas, ArrayRecipes arrayRecipes, ArrayUsers arrayUsers, FrmReporteUsuarios frmTablaUsuarios, FrmReporteRecetas frmTablaRecetas) {
        this.frmInicio = frmInicio;
        this.frmInicioSesion = frmInicioSesion;
        this.frmLista = frmLista;
        this.frmRegistro = frmRegistro;
        this.frmRecetas = frmRecetas;
        this.arrayRecipes = arrayRecipes;
        this.arrayUsers = arrayUsers;
        this.frmTablaUsuarios = frmTablaUsuarios;
        this.frmTablaRecetas = frmTablaRecetas;
        panel = frmLista.getPanel();
        actualizarRecetas();
        frmTablaUsuarios.escuchar(this);
        frmTablaRecetas.escuchar(this);
    }//Fin constructor

    public void setPrivilegedAccess(boolean privilegedAccess) {
        this.privilegedAccess = privilegedAccess;
    }//Fin del setPrivilegedAccess

    public void setAccess(boolean access) {
        this.access = access;
    }//Fin del setAccess

    public boolean isRangoValido() {
        int totalRecetas = arrayRecipes.getLongitudArray();
        return (receta1 >= 0 && receta3 < totalRecetas);
    }//Fin isRangoValido

    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "volver":
                if (receta1 > 0) {
                    receta1 -= 3;
                    receta2 -= 3;
                    receta3 -= 3;
                    actualizarRecetas();
                } else {
                    frmLista.mostrarMensaje("It's not possible to return because there are no previous recipes");
                }//Fin else
                break;

            case "siguiente":
                if (receta3 < arrayRecipes.getLongitudArray() - 1) {
                    receta1 += 3;
                    receta2 += 3;
                    receta3 += 3;
                    actualizarRecetas();
                } else {
                    frmLista.mostrarMensaje("It's not possible to continue because there are no more recipes");
                }//Fin else
                break;

            case "iniciarSesion":
                System.out.println("Apretaste para ir a la ventana para iniciar sesión");
                frmLista.setVisible(false);
                frmInicioSesion.setVisible(true);
                break;

            case "registrarse":
                System.out.println("Apretaste para ir a la ventana para registrarse");
                frmLista.setVisible(false);
                frmRegistro.setVisible(true);
                break;

            case "lista":
                if (privilegedAccess || access) {
                    System.out.println("Apretaste para ir a la ventana para ver la lista");
                } else {
                    frmInicioSesion.mostrarMensaje("First you have to log in to use this option");
                }
                break;

            case "manipularReceta":
                if (privilegedAccess) {
                    System.out.println("Apretaste para ir a la ventana para manipular recetas");
                    frmLista.setVisible(false);
                    frmRecetas.setVisible(true);
                } else {
                    frmInicioSesion.mostrarMensaje("Only users can access this option");
                }
                break;

            case "Tabla Usuarios":
                if (privilegedAccess) {
                    frmTablaUsuarios.setTabla(Users.ETIQUETAS_USER, arrayUsers.reporteTabla());
                    frmTablaUsuarios.setVisible(true);
                    frmLista.setVisible(false);
                } else {
                    frmInicioSesion.mostrarMensaje("Only users can access this option");
                }//Fin else
                break;

            case "Tabla Recetas":
                if (privilegedAccess || access) {
                    frmTablaRecetas.setTabla(Recipes.ETIQUETAS_RECIPES, arrayRecipes.reporteTabla());
                    frmTablaRecetas.setVisible(true);
                    frmLista.setVisible(false);
                } else {
                    frmInicioSesion.mostrarMensaje("Only users can access this option");
                }//Fin else
                break;
        }//Fin switch
    }//Fin actionPerformed

    public void actualizarRecetas() {
        if (arrayRecipes.getLongitudArray() == 0) {
            frmLista.mostrarMensaje("No data can be shown because no prescriptions have been registered");
        }//Fin if
        else {
            if (arrayRecipes.getReceta(receta1) != null) {
                panel.setTxtNombre1(arrayRecipes.getReceta(receta1).getName() + " (" + arrayRecipes.getReceta(receta1).getId() + ")");
                panel.setImagen1(arrayRecipes.getReceta(receta1).getImage());
            }//Fin if
            else {
                panel.setTxtNombre1("");
                panel.setImagen1("limpiar");
            }//Fin else

            if (arrayRecipes.getReceta(receta2) != null) {
                panel.setTxtNombre2(arrayRecipes.getReceta(receta2).getName() + " (" + arrayRecipes.getReceta(receta2).getId() + ")");
                panel.setImagen2(arrayRecipes.getReceta(receta2).getImage());
            }//Fin if
            else {
                panel.setTxtNombre2("");
                panel.setImagen2("limpiar");
            }//Fin else

            if (arrayRecipes.getReceta(receta3) != null) {
                panel.setTxtNombre3(arrayRecipes.getReceta(receta3).getName() + " (" + arrayRecipes.getReceta(receta3).getId() + ")");
                panel.setImagen3(arrayRecipes.getReceta(receta3).getImage());
            }//Fin if
            else {
                panel.setTxtNombre3("");
                panel.setImagen3("limpiar");
            }//Fin else
        }//Fin else
    }//Fin actualizarRecetas

}//Fin clase

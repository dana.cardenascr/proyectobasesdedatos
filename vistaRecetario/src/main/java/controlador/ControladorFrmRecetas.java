/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Persistence;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import modelo.ArrayRecipes;
import modelo.ArrayUsers;
import modelo.Levels;
import modelo.LevelsJpaController;
import modelo.Recipes;
import modelo.RecipesJpaController;
import modelo.Users;
import modelo.exceptions.IllegalOrphanException;
import modelo.exceptions.NonexistentEntityException;
import vista.FrmInicio;
import vista.FrmInicioSesion;
import vista.FrmListaRecetas;
import vista.FrmRecetas;
import vista.FrmRegistro;
import vista.FrmReporteRecetas;
import vista.FrmReporteUsuarios;
import vista.JpnlDatosRecetas;

/**
 *
 * @author Ashley
 */
public class ControladorFrmRecetas implements ActionListener {

    private FrmInicio frmInicio;
    private FrmInicioSesion frmInicioSesion;
    private FrmListaRecetas frmLista;
    private FrmRegistro frmRegistro;
    private FrmRecetas frmRecetas;
    private JpnlDatosRecetas panelDatos;
    private ArrayRecipes arregloRecetas;
    private ArrayUsers arrayUsers;
    private FrmReporteUsuarios frmTablaUsuarios;
    private FrmReporteRecetas frmTablaRecetas;
    private boolean privilegedAccess;
    private boolean access;
    Levels level;

    public ControladorFrmRecetas(FrmInicio frmInicio, FrmInicioSesion frmInicioSesion, FrmListaRecetas frmLista, FrmRegistro frmRegistro, FrmRecetas frmRecetas) {
        this.frmRecetas = frmRecetas;
        this.frmInicio = frmInicio;
        this.frmInicioSesion = frmInicioSesion;
        this.frmLista = frmLista;
        this.frmRegistro = frmRegistro;
        panelDatos = frmRecetas.getPanel();
        arregloRecetas = new ArrayRecipes();
    }//Fin constructor

    public ControladorFrmRecetas(FrmInicio frmInicio, FrmInicioSesion frmInicioSesion, FrmListaRecetas frmLista, FrmRegistro frmRegistro, FrmRecetas frmRecetas, ArrayRecipes arregloRecetas, ArrayUsers arrayUsers, FrmReporteUsuarios frmTablaUsuarios, FrmReporteRecetas frmTablaRecetas) {
        this.frmInicio = frmInicio;
        this.frmInicioSesion = frmInicioSesion;
        this.frmLista = frmLista;
        this.frmRegistro = frmRegistro;
        this.frmRecetas = frmRecetas;
        this.arregloRecetas = arregloRecetas;
        this.arrayUsers = arrayUsers;
        this.frmTablaUsuarios = frmTablaUsuarios;
        this.frmTablaRecetas = frmTablaRecetas;
        panelDatos = frmRecetas.getPanel();
        frmTablaUsuarios.escuchar(this);
        frmTablaRecetas.escuchar(this);
    }//Fin constructor

    public boolean isDatosIncompletos() {
        if ((panelDatos.getTxtNombreReceta().equals("Nada")) || (panelDatos.getTxtPorciones() == -1) || (panelDatos.getTxtTiempoCoccion() == -1.0f) || (panelDatos.getTxtTiempoPreparacion() == -1.0f) || (panelDatos.getTxtTiempoTotal() == -1.0f) || (panelDatos.getIndiceSeleccionado("complejidad") == 0) || (panelDatos.getTxtInstrucciones().equals("Nada")) || (panelDatos.getTxtDescripcion().equals("Nada")) || (panelDatos.getImagePath().equals(""))) {
            return true;
        }//Fin if
        return false;
    }//Fin isDatosIncompletos
    
    // (panelDatos.getTxtId() == -1)

    public void setPrivilegedAccess(boolean privilegedAccess) {
        this.privilegedAccess = privilegedAccess;
    }//Fin del setPrivilegedAccess

    public void setAccess(boolean access) {
        this.access = access;
    }//Fin del setAccess

    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "agregarImagen":
                JFileChooser selectorDeArchivos = new JFileChooser();
                File archivo;
                String ruta = "";
                selectorDeArchivos.setFileSelectionMode(JFileChooser.FILES_ONLY);
                if (selectorDeArchivos.showOpenDialog(selectorDeArchivos) == 0) {
                    archivo = selectorDeArchivos.getSelectedFile();
                    ruta = archivo.getPath();
                }//Fin if
                panelDatos.setImagen(ruta);
                System.out.println("Apretaste el botón para agregar una imagen");
                break;

            case "buscar":
                if (panelDatos.getTxtId() == -1) {
                    frmRecetas.mostrarMensaje("Complete all the data before proceeding");
                } else {
                    if (arregloRecetas.buscar(panelDatos.getTxtId()) != null) {
                        RecipesJpaController jpaRecipes = new RecipesJpaController(Persistence.createEntityManagerFactory("persistenciaBD"));
                        Recipes recipe = jpaRecipes.findRecipes(panelDatos.getTxtId());
                        panelDatos.setTxtNombreReceta(recipe.getName());
                        panelDatos.setTxtTiempoCoccion(recipe.getCookingTime());
                        panelDatos.setTxtDescripcion(recipe.getDescription());
                        panelDatos.setComplejidad(recipe.getLevelsId().getLevel());
                        panelDatos.setImagen(recipe.getImage());
                        panelDatos.setTxtPorciones(recipe.getPortions());
                        panelDatos.setTxtInstrucciones(recipe.getPreparationInstructions());
                        panelDatos.setTxtTiempoPreparacion(recipe.getPreparationTime());
                        panelDatos.setTxtTiempoTotal(recipe.getTotalTime());
                    }//Fin else
                    else {
                        frmRecetas.mostrarMensaje("This recipe has not been registered yet");
                    }//Fin else
                }//Fin else
                break;

            case "agregar":
                if (isDatosIncompletos()) {
                    frmRecetas.mostrarMensaje("Complete all the data before proceeding");
                } else {
                    if (arregloRecetas.buscar(panelDatos.getTxtId()) == null) {
                        LevelsJpaController jpaLevels = new LevelsJpaController(Persistence.createEntityManagerFactory("persistenciaBD"));
                        if (jpaLevels.findLevelsByName(panelDatos.getComplejidad()) != null) {
                            level = jpaLevels.findLevelsByName(panelDatos.getComplejidad());
                        }//Fin if
                        else {
                            level = new Levels(0, panelDatos.getComplejidad());
                            jpaLevels.create(level);
                        }//Fin else
                        RecipesJpaController jpaRecipes = new RecipesJpaController(Persistence.createEntityManagerFactory("persistenciaBD"));
                        Recipes recipe = new Recipes(panelDatos.getTxtId(), panelDatos.getTxtTiempoCoccion(), panelDatos.getTxtDescripcion(), panelDatos.getImagePath(), panelDatos.getTxtNombreReceta(), panelDatos.getTxtPorciones(), panelDatos.getTxtInstrucciones(), panelDatos.getTxtTiempoPreparacion(), panelDatos.getTxtTiempoTotal());
                        recipe.setLevelsId(level);
                        jpaRecipes.create(recipe);
                        arregloRecetas.agregar(recipe);
                        frmRecetas.mostrarMensaje("The recipe has been successfully registered");
                        panelDatos.limpiar();
                        panelDatos.setImagen("");
                    } else {
                        frmRecetas.mostrarMensaje("This recipe has already been registered");
                    }//Fin else
                }//Fin else
                System.out.println("Apretaste el botón de agregar");
                break;

            case "modificar":
                if (isDatosIncompletos()|| (panelDatos.getTxtId() == -1)) {
                    frmRecetas.mostrarMensaje("Complete all the data before proceeding");
                } else {
                    if (arregloRecetas.buscar(panelDatos.getTxtId()) != null) {
                        LevelsJpaController jpaLevels = new LevelsJpaController(Persistence.createEntityManagerFactory("persistenciaBD"));
                        level = jpaLevels.findLevelsByName(panelDatos.getComplejidad());
                        try {
                            RecipesJpaController jpaRecipes = new RecipesJpaController(Persistence.createEntityManagerFactory("persistenciaBD"));
                            Recipes recipe = jpaRecipes.findRecipes(panelDatos.getTxtId());
                            recipe.setName(panelDatos.getTxtNombreReceta());
                            recipe.setCookingTime(panelDatos.getTxtTiempoCoccion());
                            recipe.setDescription(panelDatos.getTxtDescripcion());
                            recipe.setImage(panelDatos.getImagePath());
                            recipe.setPortions(panelDatos.getTxtPorciones());
                            recipe.setPreparationInstructions(panelDatos.getTxtInstrucciones());
                            recipe.setPreparationTime(panelDatos.getTxtTiempoPreparacion());
                            recipe.setTotalTime(panelDatos.getTxtTiempoTotal());

                            if (!level.getLevel().equalsIgnoreCase(panelDatos.getComplejidad())) {
                                //CAMBIARLE EL NIVEL 
                            }//Fin if

                            jpaRecipes.edit(recipe);
                            JOptionPane.showMessageDialog(null, arregloRecetas.modificar(recipe));
                            panelDatos.limpiar();
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(ControladorFrmRecetas.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(ControladorFrmRecetas.class.getName()).log(Level.SEVERE, null, ex);
                        }//Fin catch
                    } else {
                        frmRecetas.mostrarMensaje("This recipe has not been registered yet");
                    }//Fin else
                }//Fin else
                System.out.println("Apretaste el botón de modificar");
                break;

            case "eliminar":
                if (panelDatos.getTxtId() == -1) {
                    frmRecetas.mostrarMensaje("Complete all the data before proceeding");
                } else {
                    if (arregloRecetas.buscar(panelDatos.getTxtId()) != null) {
                        RecipesJpaController jpaRecipes = new RecipesJpaController(Persistence.createEntityManagerFactory("persistenciaBD"));
                        try {
                            jpaRecipes.destroy(panelDatos.getTxtId());
                            JOptionPane.showMessageDialog(null, arregloRecetas.eliminar(panelDatos.getTxtId()));
                            //frmRecetas.mostrarMensaje("La receta se ha eliminado exitosamente");
                            panelDatos.limpiar();
                        } catch (IllegalOrphanException ex) {
                            Logger.getLogger(ControladorFrmRecetas.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(ControladorFrmRecetas.class.getName()).log(Level.SEVERE, null, ex);
                        }//Fin catch
                    } else {
                        frmRecetas.mostrarMensaje("This recipe has not been registered yet");
                    }//Fin else
                }//Fin else
                System.out.println("Apretaste el botón de eliminar");
                break;

            case "iniciarSesion":
                System.out.println("Apretaste para ir a la ventana para iniciar sesión");
                frmRecetas.setVisible(false);
                frmInicioSesion.setVisible(true);
                panelDatos.limpiar();
                break;

            case "registrarse":
                System.out.println("Apretaste para ir a la ventana para registrarse");
                frmRecetas.setVisible(false);
                frmRegistro.setVisible(true);
                panelDatos.limpiar();
                break;

            case "lista":
                if (privilegedAccess || access) {
                    System.out.println("Apretaste para ir a la ventana para ver la lista");
                    frmRecetas.setVisible(false);
                    frmLista.setVisible(true);
                    panelDatos.limpiar();
                } else {
                    frmInicioSesion.mostrarMensaje("First you have to log in to use this option");
                }
                break;

            case "manipularReceta":
                if (privilegedAccess) {
                    System.out.println("Apretaste para ir a la ventana para manipular recetas");
                    panelDatos.limpiar();
                } else {
                    frmInicioSesion.mostrarMensaje("Only users can access this option");
                }
                break;

            case "Tabla Usuarios":
                if (privilegedAccess) {
                    frmTablaUsuarios.setTabla(Users.ETIQUETAS_USER, arrayUsers.reporteTabla());
                    frmTablaUsuarios.setVisible(true);
                    frmRecetas.setVisible(false);
                } else {
                    frmInicioSesion.mostrarMensaje("Only users can access this option");
                }
                break;

            case "Tabla Recetas":
                if (privilegedAccess || access) {
                    frmTablaRecetas.setTabla(Recipes.ETIQUETAS_RECIPES, arregloRecetas.reporteTabla());
                    frmTablaRecetas.setVisible(true);
                    frmRecetas.setVisible(false);
                } else {
                    frmInicioSesion.mostrarMensaje("First you have to log in to use this option");
                }
                break;

        }//Fin switch
    }//Fin actionPerformed
}//Fin clase

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import javax.persistence.Persistence;
import modelo.Levels;
import modelo.LevelsJpaController;
import modelo.Recipes;
import modelo.RecipesJpaController;
import vista.FrmInicio;

/**
 *
 * @author Ashley
 */
public class main {
    public static void main(String args[]) {
        new FrmInicio();
    }//Fin main
}//Fin clase

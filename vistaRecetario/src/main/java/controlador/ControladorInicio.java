/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.ArrayRecipes;
import modelo.ArrayUsers;
import modelo.Recipes;
import modelo.Users;
import vista.FrmInicio;
import vista.FrmInicioSesion;
import vista.FrmListaRecetas;
import vista.FrmRecetas;
import vista.FrmRegistro;
import vista.FrmReporteRecetas;
import vista.FrmReporteUsuarios;

/**
 *
 * @author Ashley
 */
public class ControladorInicio implements ActionListener {

    private FrmInicio frmInicio;
    private FrmInicioSesion frmInicioSesion;
    private FrmListaRecetas frmLista;
    private FrmRegistro frmRegistro;
    private FrmRecetas frmRecetas;
    private FrmReporteUsuarios frmTablaUsuarios;
    private FrmReporteRecetas frmTablaRecetas;
    private ArrayUsers arrayUsers;
    private ArrayRecipes arrayRecipes;
    private boolean privilegedAccess;
    private boolean access;

    public ControladorInicio(FrmInicio frmInicio, FrmInicioSesion frmInicioSesion, FrmListaRecetas frmLista, FrmRegistro frmRegistro, FrmRecetas frmRecetas) {
        this.frmInicio = frmInicio;
        this.frmInicioSesion = frmInicioSesion;
        this.frmLista = frmLista;
        this.frmRegistro = frmRegistro;
        this.frmRecetas = frmRecetas;
    }//Fin constructor

    public ControladorInicio(FrmInicio frmInicio, FrmInicioSesion frmInicioSesion, FrmListaRecetas frmLista, FrmRegistro frmRegistro, FrmRecetas frmRecetas, FrmReporteUsuarios frmTablaUsuarios, FrmReporteRecetas frmTablaRecetas, ArrayUsers arrayUsers, ArrayRecipes arrayRecipes) {
        this.frmInicio = frmInicio;
        this.frmInicioSesion = frmInicioSesion;
        this.frmLista = frmLista;
        this.frmRegistro = frmRegistro;
        this.frmRecetas = frmRecetas;
        this.frmTablaUsuarios = frmTablaUsuarios;
        this.frmTablaRecetas = frmTablaRecetas;
        this.arrayUsers = arrayUsers;
        this.arrayRecipes = arrayRecipes;
        frmTablaUsuarios.escuchar(this);
        frmTablaRecetas.escuchar(this);
    }

    public void setPrivilegedAccess(boolean privilegedAccess) {
        this.privilegedAccess = privilegedAccess;
    }//Fin del setPrivilegedAccess

    public void setAccess(boolean access) {
        this.access = access;
    }//Fin del setAccess

    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "iniciarSesion":
                System.out.println("Apretaste para ir a la ventana para iniciar sesión");
                frmInicio.setVisible(false);
                frmInicioSesion.setVisible(true);
                break;

            case "registrarse":
                System.out.println("Apretaste para ir a la ventana para registrarse");
                frmInicio.setVisible(false);
                frmRegistro.setVisible(true);
                break;

            case "lista":
                if (privilegedAccess || access) {
                    System.out.println("Apretaste para ir a la ventana para ver la lista");
                    frmInicio.setVisible(false);
                    frmLista.setVisible(true);
                } else {
                    frmInicioSesion.mostrarMensaje("First you have to log in to use this option");
                }
                break;

            case "manipularReceta":
                if (privilegedAccess) {
                    System.out.println("Apretaste para ir a la ventana para manipular recetas");
                    frmInicio.setVisible(false);
                    frmRecetas.setVisible(true);
                } else {
                    frmInicioSesion.mostrarMensaje("Only users can access this option");
                }
                break;
            case "Salir":
                System.exit(0);
                break;

            case "Tabla Usuarios":
                if (privilegedAccess) {
                    frmTablaUsuarios.setTabla(Users.ETIQUETAS_USER, arrayUsers.reporteTabla());
                    frmTablaUsuarios.setVisible(true);
                    frmInicio.setVisible(false);
                } else {
                    frmInicioSesion.mostrarMensaje("Only users can access this option");
                }
                break;

            case "Tabla Recetas":
                if (privilegedAccess || access) {
                    frmTablaRecetas.setTabla(Recipes.ETIQUETAS_RECIPES, arrayRecipes.reporteTabla());
                    frmTablaRecetas.setVisible(true);
                    frmInicio.setVisible(false);
                } else {
                    frmInicioSesion.mostrarMensaje("First you have to log in to use this option");
                }
                break;

            case "Volver":
                frmTablaRecetas.setVisible(false);
                frmInicio.setVisible(true);
                break;

            case "Volver1":
                frmTablaRecetas.setVisible(false);
                frmInicio.setVisible(true);
                break;

        }//Fin switch
    }//Fin actionPerformed

}//Fin ControladorInicio

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.persistence.Persistence;
import modelo.ArrayUsers;
import modelo.Profile;
import modelo.ProfileJpaController;
import modelo.Users;
import modelo.UsersJpaController;
import vista.FrmInicio;
import vista.FrmInicioSesion;
import vista.FrmListaRecetas;
import vista.FrmRecetas;
import vista.FrmRegistro;
import vista.JpnlRegistro;

/**
 *
 * @author Ashley
 */
public class ControladorFrmRegistro implements ActionListener {

    private FrmInicio frmInicio;
    private FrmInicioSesion frmInicioSesion;
    private FrmListaRecetas frmLista;
    private FrmRegistro frmRegistro;
    private FrmRecetas frmRecetas;
    private JpnlRegistro panel;
    private ArrayUsers arrayUsers;
    private boolean privilegedAccess;
    private boolean access;

    public ControladorFrmRegistro(FrmInicio frmInicio, FrmInicioSesion frmInicioSesion, FrmListaRecetas frmLista, FrmRegistro frmRegistro, FrmRecetas frmRecetas) {
        this.frmInicio = frmInicio;
        this.frmInicioSesion = frmInicioSesion;
        this.frmLista = frmLista;
        this.frmRegistro = frmRegistro;
        this.frmRecetas = frmRecetas;
        panel = frmRegistro.getPanel();
        arrayUsers = new ArrayUsers();
    }//Fin constructor

    public boolean isDatosIncompletos() {
        if (panel.getTxtPais().equals("Nada") || panel.getTxtCorreo().equals("Nada") || panel.getTxtApellidos().equals("Nada") || panel.getTxtNombre().equals("Nada") || panel.getTxtContrasena().equals("Nada")) {
            return true;
        } else {
            return false;
        }//Fin else
    }//Fin constructor

    public void setPrivilegedAccess(boolean privilegedAccess) {
        this.privilegedAccess = privilegedAccess;
    }//Fin del setPrivilegedAccess

    public void setAccess(boolean access) {
        this.access = access;
    }//Fin del setAccess

    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {

            case "registro":
                if (isDatosIncompletos()) {
                    frmRegistro.mostrarMensaje("Complete all the data before proceeding");
                } else {
                    if (arrayUsers.buscarUserName(panel.getTxtNombre()) == null) {
                        ProfileJpaController jpaProfile = new ProfileJpaController(Persistence.createEntityManagerFactory("persistenciaBD"));
                        Profile profile = jpaProfile.findProfile(1);
                        UsersJpaController userJpa = new UsersJpaController(Persistence.createEntityManagerFactory("persistenciaBD"));
                        Users user = new Users(arrayUsers.getId(), panel.getTxtPais(), panel.getTxtCorreo(), panel.getTxtApellidos(), panel.getTxtNombre(), panel.getTxtContrasena());
                        user.setProfileId(profile);
                        userJpa.create(user);
                        arrayUsers.agregar(user);
                        frmRegistro.mostrarMensaje("Successfully registered");
                        panel.limpiar();
                        frmInicio.setVisible(true);
                        frmRegistro.setVisible(false);
                    } else {
                        frmRegistro.mostrarMensaje("The user you are trying to register is already registered");
                    }//Fin else
                }//Fin else
                System.out.println("Apretaste el botón para registrarte");
                break;

            case "iniciarSesion":
                System.out.println("Apretaste para ir a la ventana para iniciar sesión");
                frmRegistro.setVisible(false);
                frmInicioSesion.setVisible(true);
                break;

            case "registrarse":
                System.out.println("Apretaste para ir a la ventana para registrarse");
                break;

            case "lista":
                if (privilegedAccess || access) {
                    System.out.println("Apretaste para ir a la ventana para ver la lista");
                    frmRegistro.setVisible(false);
                    frmLista.setVisible(true);
                } else {
                    frmInicioSesion.mostrarMensaje("First you have to log in to use this option");
                }
                break;

            case "manipularReceta":
                if (privilegedAccess) {
                    System.out.println("Apretaste para ir a la ventana para manipular recetas");
                    frmRegistro.setVisible(false);
                    frmRecetas.setVisible(true);
                } else {
                    frmInicioSesion.mostrarMensaje("Only users can access this option");
                }
                break;
        }//Fin switch
    }//Fin actionPerformed

}//Fin clase

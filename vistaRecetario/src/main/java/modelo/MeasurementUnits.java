/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Ashley
 */
@Entity
@Table(name = "measurement_units")
@NamedQueries({
    @NamedQuery(name = "MeasurementUnits.findAll", query = "SELECT m FROM MeasurementUnits m"),
    @NamedQuery(name = "MeasurementUnits.findById", query = "SELECT m FROM MeasurementUnits m WHERE m.id = :id"),
    @NamedQuery(name = "MeasurementUnits.findByMeasurementUnit", query = "SELECT m FROM MeasurementUnits m WHERE m.measurementUnit = :measurementUnit")})
public class MeasurementUnits implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "measurement_unit")
    private String measurementUnit;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "measurementUnits")
    private List<RecipesHasIngredients> recipesHasIngredientsList;

    public MeasurementUnits() {
    }

    public MeasurementUnits(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public List<RecipesHasIngredients> getRecipesHasIngredientsList() {
        return recipesHasIngredientsList;
    }

    public void setRecipesHasIngredientsList(List<RecipesHasIngredients> recipesHasIngredientsList) {
        this.recipesHasIngredientsList = recipesHasIngredientsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MeasurementUnits)) {
            return false;
        }
        MeasurementUnits other = (MeasurementUnits) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.MeasurementUnits[ id=" + id + " ]";
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Ashley
 */
@Entity
@Table(name = "users")
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findById", query = "SELECT u FROM Users u WHERE u.id = :id"),
    @NamedQuery(name = "Users.findByCountry", query = "SELECT u FROM Users u WHERE u.country = :country"),
    @NamedQuery(name = "Users.findByEmail", query = "SELECT u FROM Users u WHERE u.email = :email"),
    @NamedQuery(name = "Users.findByLastName", query = "SELECT u FROM Users u WHERE u.lastName = :lastName"),
    @NamedQuery(name = "Users.findByName", query = "SELECT u FROM Users u WHERE u.name = :name"),
    @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password")})
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "country")
    private String country;
    @Column(name = "email")
    private String email;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "name")
    private String name;
    @Column(name = "password")
    private String password;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private List<UsersVoteRecipes> usersVoteRecipesList;
    @JoinColumn(name = "profile_id", referencedColumnName = "id")
    @ManyToOne
    private Profile profileId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private List<UsersSaveRecipes> usersSaveRecipesList;
    
    //Comstante
    public static final String[] ETIQUETAS_USER = {"ID", "Name", "LastName", "Country",  "Email"};
    public static final int SIZE_USER = ETIQUETAS_USER.length;

    public Users() {
    }
    
    

    public Users(Integer id) {
        this.id = id;
    }

    public Users(Integer id, String country, String email, String lastName, String name, String password) {
        this.id = id;
        this.country = country;
        this.email = email;
        this.lastName = lastName;
        this.name = name;
        this.password = password;
    }

    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UsersVoteRecipes> getUsersVoteRecipesList() {
        return usersVoteRecipesList;
    }

    public void setUsersVoteRecipesList(List<UsersVoteRecipes> usersVoteRecipesList) {
        this.usersVoteRecipesList = usersVoteRecipesList;
    }

    public Profile getProfileId() {
        return profileId;
    }

    public void setProfileId(Profile profileId) {
        this.profileId = profileId;
    }

    public List<UsersSaveRecipes> getUsersSaveRecipesList() {
        return usersSaveRecipesList;
    }

    public void setUsersSaveRecipesList(List<UsersSaveRecipes> usersSaveRecipesList) {
        this.usersSaveRecipesList = usersSaveRecipesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

   @Override
    public String toString() {
        return "Users{" + "id=" + id + ", country=" + country + ", email=" + email + ", lastName=" + lastName + ", name=" + name + ", password=" + password + ", usersVoteRecipesList=" + usersVoteRecipesList + ", profileId=" + profileId + ", usersSaveRecipesList=" + usersSaveRecipesList + '}';
    }
    
     public String getDatosCelda(int posicion) {
        switch (posicion) {
            case 0:
                return this.getId() + "";
            case 1:
                return this.getName();
            case 2:
                return this.getLastName();
            case 3:
                return this.getCountry();
            case 4:
                return this.getEmail();

        }
        return null;
    }
    
}

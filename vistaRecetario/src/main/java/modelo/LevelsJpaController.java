/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import modelo.exceptions.NonexistentEntityException;

/**
 *
 * @author Ashley
 */
public class LevelsJpaController implements Serializable {

    public LevelsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Levels levels) {
        if (levels.getRecipesList() == null) {
            levels.setRecipesList(new ArrayList<Recipes>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Recipes> attachedRecipesList = new ArrayList<Recipes>();
            for (Recipes recipesListRecipesToAttach : levels.getRecipesList()) {
                recipesListRecipesToAttach = em.getReference(recipesListRecipesToAttach.getClass(), recipesListRecipesToAttach.getId());
                attachedRecipesList.add(recipesListRecipesToAttach);
            }
            levels.setRecipesList(attachedRecipesList);
            em.persist(levels);
            for (Recipes recipesListRecipes : levels.getRecipesList()) {
                Levels oldLevelsIdOfRecipesListRecipes = recipesListRecipes.getLevelsId();
                recipesListRecipes.setLevelsId(levels);
                recipesListRecipes = em.merge(recipesListRecipes);
                if (oldLevelsIdOfRecipesListRecipes != null) {
                    oldLevelsIdOfRecipesListRecipes.getRecipesList().remove(recipesListRecipes);
                    oldLevelsIdOfRecipesListRecipes = em.merge(oldLevelsIdOfRecipesListRecipes);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Levels levels) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Levels persistentLevels = em.find(Levels.class, levels.getId());
            List<Recipes> recipesListOld = persistentLevels.getRecipesList();
            List<Recipes> recipesListNew = levels.getRecipesList();
            List<Recipes> attachedRecipesListNew = new ArrayList<Recipes>();
            for (Recipes recipesListNewRecipesToAttach : recipesListNew) {
                recipesListNewRecipesToAttach = em.getReference(recipesListNewRecipesToAttach.getClass(), recipesListNewRecipesToAttach.getId());
                attachedRecipesListNew.add(recipesListNewRecipesToAttach);
            }
            recipesListNew = attachedRecipesListNew;
            levels.setRecipesList(recipesListNew);
            levels = em.merge(levels);
            for (Recipes recipesListOldRecipes : recipesListOld) {
                if (!recipesListNew.contains(recipesListOldRecipes)) {
                    recipesListOldRecipes.setLevelsId(null);
                    recipesListOldRecipes = em.merge(recipesListOldRecipes);
                }
            }
            for (Recipes recipesListNewRecipes : recipesListNew) {
                if (!recipesListOld.contains(recipesListNewRecipes)) {
                    Levels oldLevelsIdOfRecipesListNewRecipes = recipesListNewRecipes.getLevelsId();
                    recipesListNewRecipes.setLevelsId(levels);
                    recipesListNewRecipes = em.merge(recipesListNewRecipes);
                    if (oldLevelsIdOfRecipesListNewRecipes != null && !oldLevelsIdOfRecipesListNewRecipes.equals(levels)) {
                        oldLevelsIdOfRecipesListNewRecipes.getRecipesList().remove(recipesListNewRecipes);
                        oldLevelsIdOfRecipesListNewRecipes = em.merge(oldLevelsIdOfRecipesListNewRecipes);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = levels.getId();
                if (findLevels(id) == null) {
                    throw new NonexistentEntityException("The levels with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Levels levels;
            try {
                levels = em.getReference(Levels.class, id);
                levels.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The levels with id " + id + " no longer exists.", enfe);
            }
            List<Recipes> recipesList = levels.getRecipesList();
            for (Recipes recipesListRecipes : recipesList) {
                recipesListRecipes.setLevelsId(null);
                recipesListRecipes = em.merge(recipesListRecipes);
            }
            em.remove(levels);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Levels> findLevelsEntities() {
        return findLevelsEntities(true, -1, -1);
    }

    public List<Levels> findLevelsEntities(int maxResults, int firstResult) {
        return findLevelsEntities(false, maxResults, firstResult);
    }

    private List<Levels> findLevelsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Levels.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Levels findLevels(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Levels.class, id);
        } finally {
            em.close();
        }
    }

    public int getLevelsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Levels> rt = cq.from(Levels.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public Levels findLevelsByName(String level) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Levels> cq = cb.createQuery(Levels.class);
            Root<Levels> root = cq.from(Levels.class);
            cq.select(root).where(cb.equal(root.get("level"), level));
            Query q = em.createQuery(cq);
            List<Levels> resultList = q.getResultList();
            if (resultList.isEmpty()) {
                return null;
            } else {
                return resultList.get(0);
            }//Fin else
        } finally {
            em.close();
        }//Fin finally
    }//Fin findLevelsByName

}

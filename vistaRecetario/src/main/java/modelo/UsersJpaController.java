/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.exceptions.IllegalOrphanException;
import modelo.exceptions.NonexistentEntityException;

/**
 *
 * @author Ashley
 */
public class UsersJpaController implements Serializable {

    public UsersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Users users) {
        if (users.getUsersVoteRecipesList() == null) {
            users.setUsersVoteRecipesList(new ArrayList<UsersVoteRecipes>());
        }
        if (users.getUsersSaveRecipesList() == null) {
            users.setUsersSaveRecipesList(new ArrayList<UsersSaveRecipes>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Profile profileId = users.getProfileId();
            if (profileId != null) {
                profileId = em.getReference(profileId.getClass(), profileId.getId());
                users.setProfileId(profileId);
            }
            List<UsersVoteRecipes> attachedUsersVoteRecipesList = new ArrayList<UsersVoteRecipes>();
            for (UsersVoteRecipes usersVoteRecipesListUsersVoteRecipesToAttach : users.getUsersVoteRecipesList()) {
                usersVoteRecipesListUsersVoteRecipesToAttach = em.getReference(usersVoteRecipesListUsersVoteRecipesToAttach.getClass(), usersVoteRecipesListUsersVoteRecipesToAttach.getUsersVoteRecipesPK());
                attachedUsersVoteRecipesList.add(usersVoteRecipesListUsersVoteRecipesToAttach);
            }
            users.setUsersVoteRecipesList(attachedUsersVoteRecipesList);
            List<UsersSaveRecipes> attachedUsersSaveRecipesList = new ArrayList<UsersSaveRecipes>();
            for (UsersSaveRecipes usersSaveRecipesListUsersSaveRecipesToAttach : users.getUsersSaveRecipesList()) {
                usersSaveRecipesListUsersSaveRecipesToAttach = em.getReference(usersSaveRecipesListUsersSaveRecipesToAttach.getClass(), usersSaveRecipesListUsersSaveRecipesToAttach.getUsersSaveRecipesPK());
                attachedUsersSaveRecipesList.add(usersSaveRecipesListUsersSaveRecipesToAttach);
            }
            users.setUsersSaveRecipesList(attachedUsersSaveRecipesList);
            em.persist(users);
            if (profileId != null) {
                profileId.getUsersList().add(users);
                profileId = em.merge(profileId);
            }
            for (UsersVoteRecipes usersVoteRecipesListUsersVoteRecipes : users.getUsersVoteRecipesList()) {
                Users oldUsersOfUsersVoteRecipesListUsersVoteRecipes = usersVoteRecipesListUsersVoteRecipes.getUsers();
                usersVoteRecipesListUsersVoteRecipes.setUsers(users);
                usersVoteRecipesListUsersVoteRecipes = em.merge(usersVoteRecipesListUsersVoteRecipes);
                if (oldUsersOfUsersVoteRecipesListUsersVoteRecipes != null) {
                    oldUsersOfUsersVoteRecipesListUsersVoteRecipes.getUsersVoteRecipesList().remove(usersVoteRecipesListUsersVoteRecipes);
                    oldUsersOfUsersVoteRecipesListUsersVoteRecipes = em.merge(oldUsersOfUsersVoteRecipesListUsersVoteRecipes);
                }
            }
            for (UsersSaveRecipes usersSaveRecipesListUsersSaveRecipes : users.getUsersSaveRecipesList()) {
                Users oldUsersOfUsersSaveRecipesListUsersSaveRecipes = usersSaveRecipesListUsersSaveRecipes.getUsers();
                usersSaveRecipesListUsersSaveRecipes.setUsers(users);
                usersSaveRecipesListUsersSaveRecipes = em.merge(usersSaveRecipesListUsersSaveRecipes);
                if (oldUsersOfUsersSaveRecipesListUsersSaveRecipes != null) {
                    oldUsersOfUsersSaveRecipesListUsersSaveRecipes.getUsersSaveRecipesList().remove(usersSaveRecipesListUsersSaveRecipes);
                    oldUsersOfUsersSaveRecipesListUsersSaveRecipes = em.merge(oldUsersOfUsersSaveRecipesListUsersSaveRecipes);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Users users) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users persistentUsers = em.find(Users.class, users.getId());
            Profile profileIdOld = persistentUsers.getProfileId();
            Profile profileIdNew = users.getProfileId();
            List<UsersVoteRecipes> usersVoteRecipesListOld = persistentUsers.getUsersVoteRecipesList();
            List<UsersVoteRecipes> usersVoteRecipesListNew = users.getUsersVoteRecipesList();
            List<UsersSaveRecipes> usersSaveRecipesListOld = persistentUsers.getUsersSaveRecipesList();
            List<UsersSaveRecipes> usersSaveRecipesListNew = users.getUsersSaveRecipesList();
            List<String> illegalOrphanMessages = null;
            for (UsersVoteRecipes usersVoteRecipesListOldUsersVoteRecipes : usersVoteRecipesListOld) {
                if (!usersVoteRecipesListNew.contains(usersVoteRecipesListOldUsersVoteRecipes)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UsersVoteRecipes " + usersVoteRecipesListOldUsersVoteRecipes + " since its users field is not nullable.");
                }
            }
            for (UsersSaveRecipes usersSaveRecipesListOldUsersSaveRecipes : usersSaveRecipesListOld) {
                if (!usersSaveRecipesListNew.contains(usersSaveRecipesListOldUsersSaveRecipes)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UsersSaveRecipes " + usersSaveRecipesListOldUsersSaveRecipes + " since its users field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (profileIdNew != null) {
                profileIdNew = em.getReference(profileIdNew.getClass(), profileIdNew.getId());
                users.setProfileId(profileIdNew);
            }
            List<UsersVoteRecipes> attachedUsersVoteRecipesListNew = new ArrayList<UsersVoteRecipes>();
            for (UsersVoteRecipes usersVoteRecipesListNewUsersVoteRecipesToAttach : usersVoteRecipesListNew) {
                usersVoteRecipesListNewUsersVoteRecipesToAttach = em.getReference(usersVoteRecipesListNewUsersVoteRecipesToAttach.getClass(), usersVoteRecipesListNewUsersVoteRecipesToAttach.getUsersVoteRecipesPK());
                attachedUsersVoteRecipesListNew.add(usersVoteRecipesListNewUsersVoteRecipesToAttach);
            }
            usersVoteRecipesListNew = attachedUsersVoteRecipesListNew;
            users.setUsersVoteRecipesList(usersVoteRecipesListNew);
            List<UsersSaveRecipes> attachedUsersSaveRecipesListNew = new ArrayList<UsersSaveRecipes>();
            for (UsersSaveRecipes usersSaveRecipesListNewUsersSaveRecipesToAttach : usersSaveRecipesListNew) {
                usersSaveRecipesListNewUsersSaveRecipesToAttach = em.getReference(usersSaveRecipesListNewUsersSaveRecipesToAttach.getClass(), usersSaveRecipesListNewUsersSaveRecipesToAttach.getUsersSaveRecipesPK());
                attachedUsersSaveRecipesListNew.add(usersSaveRecipesListNewUsersSaveRecipesToAttach);
            }
            usersSaveRecipesListNew = attachedUsersSaveRecipesListNew;
            users.setUsersSaveRecipesList(usersSaveRecipesListNew);
            users = em.merge(users);
            if (profileIdOld != null && !profileIdOld.equals(profileIdNew)) {
                profileIdOld.getUsersList().remove(users);
                profileIdOld = em.merge(profileIdOld);
            }
            if (profileIdNew != null && !profileIdNew.equals(profileIdOld)) {
                profileIdNew.getUsersList().add(users);
                profileIdNew = em.merge(profileIdNew);
            }
            for (UsersVoteRecipes usersVoteRecipesListNewUsersVoteRecipes : usersVoteRecipesListNew) {
                if (!usersVoteRecipesListOld.contains(usersVoteRecipesListNewUsersVoteRecipes)) {
                    Users oldUsersOfUsersVoteRecipesListNewUsersVoteRecipes = usersVoteRecipesListNewUsersVoteRecipes.getUsers();
                    usersVoteRecipesListNewUsersVoteRecipes.setUsers(users);
                    usersVoteRecipesListNewUsersVoteRecipes = em.merge(usersVoteRecipesListNewUsersVoteRecipes);
                    if (oldUsersOfUsersVoteRecipesListNewUsersVoteRecipes != null && !oldUsersOfUsersVoteRecipesListNewUsersVoteRecipes.equals(users)) {
                        oldUsersOfUsersVoteRecipesListNewUsersVoteRecipes.getUsersVoteRecipesList().remove(usersVoteRecipesListNewUsersVoteRecipes);
                        oldUsersOfUsersVoteRecipesListNewUsersVoteRecipes = em.merge(oldUsersOfUsersVoteRecipesListNewUsersVoteRecipes);
                    }
                }
            }
            for (UsersSaveRecipes usersSaveRecipesListNewUsersSaveRecipes : usersSaveRecipesListNew) {
                if (!usersSaveRecipesListOld.contains(usersSaveRecipesListNewUsersSaveRecipes)) {
                    Users oldUsersOfUsersSaveRecipesListNewUsersSaveRecipes = usersSaveRecipesListNewUsersSaveRecipes.getUsers();
                    usersSaveRecipesListNewUsersSaveRecipes.setUsers(users);
                    usersSaveRecipesListNewUsersSaveRecipes = em.merge(usersSaveRecipesListNewUsersSaveRecipes);
                    if (oldUsersOfUsersSaveRecipesListNewUsersSaveRecipes != null && !oldUsersOfUsersSaveRecipesListNewUsersSaveRecipes.equals(users)) {
                        oldUsersOfUsersSaveRecipesListNewUsersSaveRecipes.getUsersSaveRecipesList().remove(usersSaveRecipesListNewUsersSaveRecipes);
                        oldUsersOfUsersSaveRecipesListNewUsersSaveRecipes = em.merge(oldUsersOfUsersSaveRecipesListNewUsersSaveRecipes);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = users.getId();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<UsersVoteRecipes> usersVoteRecipesListOrphanCheck = users.getUsersVoteRecipesList();
            for (UsersVoteRecipes usersVoteRecipesListOrphanCheckUsersVoteRecipes : usersVoteRecipesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the UsersVoteRecipes " + usersVoteRecipesListOrphanCheckUsersVoteRecipes + " in its usersVoteRecipesList field has a non-nullable users field.");
            }
            List<UsersSaveRecipes> usersSaveRecipesListOrphanCheck = users.getUsersSaveRecipesList();
            for (UsersSaveRecipes usersSaveRecipesListOrphanCheckUsersSaveRecipes : usersSaveRecipesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the UsersSaveRecipes " + usersSaveRecipesListOrphanCheckUsersSaveRecipes + " in its usersSaveRecipesList field has a non-nullable users field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Profile profileId = users.getProfileId();
            if (profileId != null) {
                profileId.getUsersList().remove(users);
                profileId = em.merge(profileId);
            }
            em.remove(users);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Users findUsers(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.exceptions.NonexistentEntityException;

/**
 *
 * @author Ashley
 */
public class OccasionsJpaController implements Serializable {

    public OccasionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Occasions occasions) {
        if (occasions.getRecipesList() == null) {
            occasions.setRecipesList(new ArrayList<Recipes>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Recipes> attachedRecipesList = new ArrayList<Recipes>();
            for (Recipes recipesListRecipesToAttach : occasions.getRecipesList()) {
                recipesListRecipesToAttach = em.getReference(recipesListRecipesToAttach.getClass(), recipesListRecipesToAttach.getId());
                attachedRecipesList.add(recipesListRecipesToAttach);
            }
            occasions.setRecipesList(attachedRecipesList);
            em.persist(occasions);
            for (Recipes recipesListRecipes : occasions.getRecipesList()) {
                recipesListRecipes.getOccasionsList().add(occasions);
                recipesListRecipes = em.merge(recipesListRecipes);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Occasions occasions) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Occasions persistentOccasions = em.find(Occasions.class, occasions.getId());
            List<Recipes> recipesListOld = persistentOccasions.getRecipesList();
            List<Recipes> recipesListNew = occasions.getRecipesList();
            List<Recipes> attachedRecipesListNew = new ArrayList<Recipes>();
            for (Recipes recipesListNewRecipesToAttach : recipesListNew) {
                recipesListNewRecipesToAttach = em.getReference(recipesListNewRecipesToAttach.getClass(), recipesListNewRecipesToAttach.getId());
                attachedRecipesListNew.add(recipesListNewRecipesToAttach);
            }
            recipesListNew = attachedRecipesListNew;
            occasions.setRecipesList(recipesListNew);
            occasions = em.merge(occasions);
            for (Recipes recipesListOldRecipes : recipesListOld) {
                if (!recipesListNew.contains(recipesListOldRecipes)) {
                    recipesListOldRecipes.getOccasionsList().remove(occasions);
                    recipesListOldRecipes = em.merge(recipesListOldRecipes);
                }
            }
            for (Recipes recipesListNewRecipes : recipesListNew) {
                if (!recipesListOld.contains(recipesListNewRecipes)) {
                    recipesListNewRecipes.getOccasionsList().add(occasions);
                    recipesListNewRecipes = em.merge(recipesListNewRecipes);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = occasions.getId();
                if (findOccasions(id) == null) {
                    throw new NonexistentEntityException("The occasions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Occasions occasions;
            try {
                occasions = em.getReference(Occasions.class, id);
                occasions.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The occasions with id " + id + " no longer exists.", enfe);
            }
            List<Recipes> recipesList = occasions.getRecipesList();
            for (Recipes recipesListRecipes : recipesList) {
                recipesListRecipes.getOccasionsList().remove(occasions);
                recipesListRecipes = em.merge(recipesListRecipes);
            }
            em.remove(occasions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Occasions> findOccasionsEntities() {
        return findOccasionsEntities(true, -1, -1);
    }

    public List<Occasions> findOccasionsEntities(int maxResults, int firstResult) {
        return findOccasionsEntities(false, maxResults, firstResult);
    }

    private List<Occasions> findOccasionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Occasions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Occasions findOccasions(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Occasions.class, id);
        } finally {
            em.close();
        }
    }

    public int getOccasionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Occasions> rt = cq.from(Occasions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.exceptions.NonexistentEntityException;

/**
 *
 * @author Ashley
 */
public class CategoriesJpaController implements Serializable {

    public CategoriesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Categories categories) {
        if (categories.getRecipesList() == null) {
            categories.setRecipesList(new ArrayList<Recipes>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Recipes> attachedRecipesList = new ArrayList<Recipes>();
            for (Recipes recipesListRecipesToAttach : categories.getRecipesList()) {
                recipesListRecipesToAttach = em.getReference(recipesListRecipesToAttach.getClass(), recipesListRecipesToAttach.getId());
                attachedRecipesList.add(recipesListRecipesToAttach);
            }
            categories.setRecipesList(attachedRecipesList);
            em.persist(categories);
            for (Recipes recipesListRecipes : categories.getRecipesList()) {
                recipesListRecipes.getCategoriesList().add(categories);
                recipesListRecipes = em.merge(recipesListRecipes);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Categories categories) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categories persistentCategories = em.find(Categories.class, categories.getId());
            List<Recipes> recipesListOld = persistentCategories.getRecipesList();
            List<Recipes> recipesListNew = categories.getRecipesList();
            List<Recipes> attachedRecipesListNew = new ArrayList<Recipes>();
            for (Recipes recipesListNewRecipesToAttach : recipesListNew) {
                recipesListNewRecipesToAttach = em.getReference(recipesListNewRecipesToAttach.getClass(), recipesListNewRecipesToAttach.getId());
                attachedRecipesListNew.add(recipesListNewRecipesToAttach);
            }
            recipesListNew = attachedRecipesListNew;
            categories.setRecipesList(recipesListNew);
            categories = em.merge(categories);
            for (Recipes recipesListOldRecipes : recipesListOld) {
                if (!recipesListNew.contains(recipesListOldRecipes)) {
                    recipesListOldRecipes.getCategoriesList().remove(categories);
                    recipesListOldRecipes = em.merge(recipesListOldRecipes);
                }
            }
            for (Recipes recipesListNewRecipes : recipesListNew) {
                if (!recipesListOld.contains(recipesListNewRecipes)) {
                    recipesListNewRecipes.getCategoriesList().add(categories);
                    recipesListNewRecipes = em.merge(recipesListNewRecipes);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = categories.getId();
                if (findCategories(id) == null) {
                    throw new NonexistentEntityException("The categories with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categories categories;
            try {
                categories = em.getReference(Categories.class, id);
                categories.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The categories with id " + id + " no longer exists.", enfe);
            }
            List<Recipes> recipesList = categories.getRecipesList();
            for (Recipes recipesListRecipes : recipesList) {
                recipesListRecipes.getCategoriesList().remove(categories);
                recipesListRecipes = em.merge(recipesListRecipes);
            }
            em.remove(categories);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Categories> findCategoriesEntities() {
        return findCategoriesEntities(true, -1, -1);
    }

    public List<Categories> findCategoriesEntities(int maxResults, int firstResult) {
        return findCategoriesEntities(false, maxResults, firstResult);
    }

    private List<Categories> findCategoriesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Categories.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Categories findCategories(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Categories.class, id);
        } finally {
            em.close();
        }
    }

    public int getCategoriesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Categories> rt = cq.from(Categories.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

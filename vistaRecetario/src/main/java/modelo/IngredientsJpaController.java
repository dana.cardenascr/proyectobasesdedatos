/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.exceptions.IllegalOrphanException;
import modelo.exceptions.NonexistentEntityException;

/**
 *
 * @author Ashley
 */
public class IngredientsJpaController implements Serializable {

    public IngredientsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ingredients ingredients) {
        if (ingredients.getRecipesHasIngredientsList() == null) {
            ingredients.setRecipesHasIngredientsList(new ArrayList<RecipesHasIngredients>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<RecipesHasIngredients> attachedRecipesHasIngredientsList = new ArrayList<RecipesHasIngredients>();
            for (RecipesHasIngredients recipesHasIngredientsListRecipesHasIngredientsToAttach : ingredients.getRecipesHasIngredientsList()) {
                recipesHasIngredientsListRecipesHasIngredientsToAttach = em.getReference(recipesHasIngredientsListRecipesHasIngredientsToAttach.getClass(), recipesHasIngredientsListRecipesHasIngredientsToAttach.getRecipesHasIngredientsPK());
                attachedRecipesHasIngredientsList.add(recipesHasIngredientsListRecipesHasIngredientsToAttach);
            }
            ingredients.setRecipesHasIngredientsList(attachedRecipesHasIngredientsList);
            em.persist(ingredients);
            for (RecipesHasIngredients recipesHasIngredientsListRecipesHasIngredients : ingredients.getRecipesHasIngredientsList()) {
                Ingredients oldIngredientsOfRecipesHasIngredientsListRecipesHasIngredients = recipesHasIngredientsListRecipesHasIngredients.getIngredients();
                recipesHasIngredientsListRecipesHasIngredients.setIngredients(ingredients);
                recipesHasIngredientsListRecipesHasIngredients = em.merge(recipesHasIngredientsListRecipesHasIngredients);
                if (oldIngredientsOfRecipesHasIngredientsListRecipesHasIngredients != null) {
                    oldIngredientsOfRecipesHasIngredientsListRecipesHasIngredients.getRecipesHasIngredientsList().remove(recipesHasIngredientsListRecipesHasIngredients);
                    oldIngredientsOfRecipesHasIngredientsListRecipesHasIngredients = em.merge(oldIngredientsOfRecipesHasIngredientsListRecipesHasIngredients);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ingredients ingredients) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingredients persistentIngredients = em.find(Ingredients.class, ingredients.getId());
            List<RecipesHasIngredients> recipesHasIngredientsListOld = persistentIngredients.getRecipesHasIngredientsList();
            List<RecipesHasIngredients> recipesHasIngredientsListNew = ingredients.getRecipesHasIngredientsList();
            List<String> illegalOrphanMessages = null;
            for (RecipesHasIngredients recipesHasIngredientsListOldRecipesHasIngredients : recipesHasIngredientsListOld) {
                if (!recipesHasIngredientsListNew.contains(recipesHasIngredientsListOldRecipesHasIngredients)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RecipesHasIngredients " + recipesHasIngredientsListOldRecipesHasIngredients + " since its ingredients field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<RecipesHasIngredients> attachedRecipesHasIngredientsListNew = new ArrayList<RecipesHasIngredients>();
            for (RecipesHasIngredients recipesHasIngredientsListNewRecipesHasIngredientsToAttach : recipesHasIngredientsListNew) {
                recipesHasIngredientsListNewRecipesHasIngredientsToAttach = em.getReference(recipesHasIngredientsListNewRecipesHasIngredientsToAttach.getClass(), recipesHasIngredientsListNewRecipesHasIngredientsToAttach.getRecipesHasIngredientsPK());
                attachedRecipesHasIngredientsListNew.add(recipesHasIngredientsListNewRecipesHasIngredientsToAttach);
            }
            recipesHasIngredientsListNew = attachedRecipesHasIngredientsListNew;
            ingredients.setRecipesHasIngredientsList(recipesHasIngredientsListNew);
            ingredients = em.merge(ingredients);
            for (RecipesHasIngredients recipesHasIngredientsListNewRecipesHasIngredients : recipesHasIngredientsListNew) {
                if (!recipesHasIngredientsListOld.contains(recipesHasIngredientsListNewRecipesHasIngredients)) {
                    Ingredients oldIngredientsOfRecipesHasIngredientsListNewRecipesHasIngredients = recipesHasIngredientsListNewRecipesHasIngredients.getIngredients();
                    recipesHasIngredientsListNewRecipesHasIngredients.setIngredients(ingredients);
                    recipesHasIngredientsListNewRecipesHasIngredients = em.merge(recipesHasIngredientsListNewRecipesHasIngredients);
                    if (oldIngredientsOfRecipesHasIngredientsListNewRecipesHasIngredients != null && !oldIngredientsOfRecipesHasIngredientsListNewRecipesHasIngredients.equals(ingredients)) {
                        oldIngredientsOfRecipesHasIngredientsListNewRecipesHasIngredients.getRecipesHasIngredientsList().remove(recipesHasIngredientsListNewRecipesHasIngredients);
                        oldIngredientsOfRecipesHasIngredientsListNewRecipesHasIngredients = em.merge(oldIngredientsOfRecipesHasIngredientsListNewRecipesHasIngredients);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ingredients.getId();
                if (findIngredients(id) == null) {
                    throw new NonexistentEntityException("The ingredients with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingredients ingredients;
            try {
                ingredients = em.getReference(Ingredients.class, id);
                ingredients.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingredients with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<RecipesHasIngredients> recipesHasIngredientsListOrphanCheck = ingredients.getRecipesHasIngredientsList();
            for (RecipesHasIngredients recipesHasIngredientsListOrphanCheckRecipesHasIngredients : recipesHasIngredientsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ingredients (" + ingredients + ") cannot be destroyed since the RecipesHasIngredients " + recipesHasIngredientsListOrphanCheckRecipesHasIngredients + " in its recipesHasIngredientsList field has a non-nullable ingredients field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(ingredients);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ingredients> findIngredientsEntities() {
        return findIngredientsEntities(true, -1, -1);
    }

    public List<Ingredients> findIngredientsEntities(int maxResults, int firstResult) {
        return findIngredientsEntities(false, maxResults, firstResult);
    }

    private List<Ingredients> findIngredientsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ingredients.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ingredients findIngredients(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ingredients.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngredientsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ingredients> rt = cq.from(Ingredients.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

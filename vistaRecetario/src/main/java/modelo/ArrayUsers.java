/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author dana
 */
public class ArrayUsers {

    private ArrayList<Users> arrayUsers;
    private JSONObject baseJSONUsers;
    private File archivo;

    public ArrayUsers() {
        arrayUsers = new ArrayList<Users>();//ArrayList<>()
        archivo = new File("users.json");
        leerJSON();
    }//Fin constructor 

    public void escribirJSON() {
        JSONArray arregloUsers = new JSONArray();
        baseJSONUsers = new JSONObject();

        for (Users user : arrayUsers) {
            JSONObject objJSONUsers = new JSONObject();
            objJSONUsers.put("id", user.getId());
            objJSONUsers.put("country", user.getCountry());
            objJSONUsers.put("email", user.getEmail());
            objJSONUsers.put("lastName", user.getLastName());
            objJSONUsers.put("name", user.getName());
            objJSONUsers.put("password", user.getPassword());
            arregloUsers.add(objJSONUsers);
        }
        baseJSONUsers.put("listUser", arregloUsers);
        try {
            FileWriter escribir = new FileWriter(archivo);
            escribir.write(baseJSONUsers.toJSONString());
            escribir.flush();
            escribir.close();
        } catch (IOException ex) {
            System.out.println("Hubo un error en el archivo");
        }//Fin catch
    }//Fin escribir
    
    public int getId(){
        return arrayUsers.size()+1;
    }//Fin getId

    public void leerJSON() {
        JSONParser parseador = new JSONParser();
        try {
            FileReader leer = new FileReader(archivo);
            Object obj = parseador.parse(leer);
            baseJSONUsers = (JSONObject) obj;
            JSONArray arregloUsers = (JSONArray) baseJSONUsers.get("listUser");
            for (Object object : arregloUsers) {
                JSONObject objJSONUser = (JSONObject) object;
                Users user = new Users(Integer.parseInt(objJSONUser.get("id").toString()), objJSONUser.get("country").toString(), objJSONUser.get("email").toString(), objJSONUser.get("lastName").toString(), objJSONUser.get("name").toString(), objJSONUser.get("password").toString());
                arrayUsers.add(user);
            }//Fin for
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParseException ex) {
            System.out.println("Hubo un error al leer el archivo");
        }//Fin catch
    }//Fin leer

    public String agregar(Users user) {
        for (int indice = 0; indice < arrayUsers.size(); indice++) {
            if ((arrayUsers.get(indice)).getId() == user.getId()) {
                return "El user ya fue agregado";
            }//Fin if
        }//Fin for
        if (arrayUsers.add(user)) {
            escribirJSON();
            return "El user fue ingresado exitosamente";
        }//Fin if
        else {
            return "Hubo un error al ingresar el user";
        }//Fin else
    }//Fin agregar

      public String modificar(Users user) {
        if (buscar(user.getId()) != null) {
            Users userOriginal = buscar(user.getId());
            userOriginal.setName(user.getName());
            userOriginal.setCountry(user.getCountry());
            userOriginal.setEmail(user.getEmail());
            userOriginal.setLastName(user.getLastName());
            userOriginal.setPassword(user.getPassword()); 
            escribirJSON();
            return "El user fue modificado correctamente";
        }//Fin if
        return "El user no ha sido ingresado";
    }//Fin del modificar

    public String eliminar(Users user) {
        if (arrayUsers.remove(user)) {
            escribirJSON();
            return "El user fue eliminado exitosamente";
        }//Fin if
        else {
            return "Hubo un error al eliminar el user";
        }//Fin else
    }//Fin eliminar

    public Users buscar(int id) {
        for (int indice = 0; indice < arrayUsers.size(); indice++) {
            if ((arrayUsers.get(indice)).getId() == id) {
                return arrayUsers.get(indice);
            }//Fin if
        }//Fin for 
        return null;
    }//Fin buscar
    
     public Users buscarUserName(String userName) {
        for (int indice = 0; indice < arrayUsers.size(); indice++) {
            if ((arrayUsers.get(indice)).getName().equals(userName)  ) {
                return arrayUsers.get(indice);
            }//Fin if
        }//Fin for 
        return null;
    }//Fin buscar

    public String toString() {
        String datos = "Los users registrados son: \n";
        for (int indice = 0; indice < arrayUsers.size(); indice++) {
            datos += arrayUsers.get(indice).toString() + "\n";
        }//Fin for
        return datos;
    }//Fin toString
    
      public String[][] reporteTabla() {
        String[][] matrizTabla = new String[this.arrayUsers.size()][Users.SIZE_USER];
        for (int fila = 0; fila < matrizTabla.length; fila++) {
            for (int columna = 0; columna < matrizTabla[fila].length; columna++) {
                matrizTabla[fila][columna] = this.arrayUsers.get(fila).getDatosCelda(columna);
            }//Fin for
        }//Fin for
        return matrizTabla;
    }//Fin reporteTabla

}//Fin clase

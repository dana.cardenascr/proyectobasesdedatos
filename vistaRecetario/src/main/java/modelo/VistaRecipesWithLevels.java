/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "vista_recipes_with_levels")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VistaRecipesWithLevels.findAll", query = "SELECT v FROM VistaRecipesWithLevels v"),
    @NamedQuery(name = "VistaRecipesWithLevels.findById", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.id = :id"),
    @NamedQuery(name = "VistaRecipesWithLevels.findByCookingTime", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.cookingTime = :cookingTime"),
    @NamedQuery(name = "VistaRecipesWithLevels.findByDescription", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.description = :description"),
    @NamedQuery(name = "VistaRecipesWithLevels.findByImage", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.image = :image"),
    @NamedQuery(name = "VistaRecipesWithLevels.findByName", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.name = :name"),
    @NamedQuery(name = "VistaRecipesWithLevels.findByPortions", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.portions = :portions"),
    @NamedQuery(name = "VistaRecipesWithLevels.findByPreparationInstructions", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.preparationInstructions = :preparationInstructions"),
    @NamedQuery(name = "VistaRecipesWithLevels.findByPreparationTime", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.preparationTime = :preparationTime"),
    @NamedQuery(name = "VistaRecipesWithLevels.findByTotalTime", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.totalTime = :totalTime"),
    @NamedQuery(name = "VistaRecipesWithLevels.findByLevel", query = "SELECT v FROM VistaRecipesWithLevels v WHERE v.level = :level")
})
public class VistaRecipesWithLevels implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private int id;

    @Column(name = "cooking_time")
    private Float cookingTime;

    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String image;

    @Column(name = "name")
    private String name;

    @Column(name = "portions")
    private Integer portions;

    @Column(name = "preparation_instructions")
    private String preparationInstructions;

    @Column(name = "preparation_time")
    private Float preparationTime;

    @Column(name = "total_time")
    private Float totalTime;

    @Column(name = "level")
    private String level;

    public VistaRecipesWithLevels() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Float getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(Float cookingTime) {
        this.cookingTime = cookingTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPortions() {
        return portions;
    }

    public void setPortions(Integer portions) {
        this.portions = portions;
    }

    public String getPreparationInstructions() {
        return preparationInstructions;
    }

    public void setPreparationInstructions(String preparationInstructions) {
        this.preparationInstructions = preparationInstructions;
    }

    public Float getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Float preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Float getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Float totalTime) {
        this.totalTime = totalTime;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}

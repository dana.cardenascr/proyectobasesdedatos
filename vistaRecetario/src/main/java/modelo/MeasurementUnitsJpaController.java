/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.exceptions.IllegalOrphanException;
import modelo.exceptions.NonexistentEntityException;

/**
 *
 * @author Ashley
 */
public class MeasurementUnitsJpaController implements Serializable {

    public MeasurementUnitsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MeasurementUnits measurementUnits) {
        if (measurementUnits.getRecipesHasIngredientsList() == null) {
            measurementUnits.setRecipesHasIngredientsList(new ArrayList<RecipesHasIngredients>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<RecipesHasIngredients> attachedRecipesHasIngredientsList = new ArrayList<RecipesHasIngredients>();
            for (RecipesHasIngredients recipesHasIngredientsListRecipesHasIngredientsToAttach : measurementUnits.getRecipesHasIngredientsList()) {
                recipesHasIngredientsListRecipesHasIngredientsToAttach = em.getReference(recipesHasIngredientsListRecipesHasIngredientsToAttach.getClass(), recipesHasIngredientsListRecipesHasIngredientsToAttach.getRecipesHasIngredientsPK());
                attachedRecipesHasIngredientsList.add(recipesHasIngredientsListRecipesHasIngredientsToAttach);
            }
            measurementUnits.setRecipesHasIngredientsList(attachedRecipesHasIngredientsList);
            em.persist(measurementUnits);
            for (RecipesHasIngredients recipesHasIngredientsListRecipesHasIngredients : measurementUnits.getRecipesHasIngredientsList()) {
                MeasurementUnits oldMeasurementUnitsOfRecipesHasIngredientsListRecipesHasIngredients = recipesHasIngredientsListRecipesHasIngredients.getMeasurementUnits();
                recipesHasIngredientsListRecipesHasIngredients.setMeasurementUnits(measurementUnits);
                recipesHasIngredientsListRecipesHasIngredients = em.merge(recipesHasIngredientsListRecipesHasIngredients);
                if (oldMeasurementUnitsOfRecipesHasIngredientsListRecipesHasIngredients != null) {
                    oldMeasurementUnitsOfRecipesHasIngredientsListRecipesHasIngredients.getRecipesHasIngredientsList().remove(recipesHasIngredientsListRecipesHasIngredients);
                    oldMeasurementUnitsOfRecipesHasIngredientsListRecipesHasIngredients = em.merge(oldMeasurementUnitsOfRecipesHasIngredientsListRecipesHasIngredients);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MeasurementUnits measurementUnits) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MeasurementUnits persistentMeasurementUnits = em.find(MeasurementUnits.class, measurementUnits.getId());
            List<RecipesHasIngredients> recipesHasIngredientsListOld = persistentMeasurementUnits.getRecipesHasIngredientsList();
            List<RecipesHasIngredients> recipesHasIngredientsListNew = measurementUnits.getRecipesHasIngredientsList();
            List<String> illegalOrphanMessages = null;
            for (RecipesHasIngredients recipesHasIngredientsListOldRecipesHasIngredients : recipesHasIngredientsListOld) {
                if (!recipesHasIngredientsListNew.contains(recipesHasIngredientsListOldRecipesHasIngredients)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RecipesHasIngredients " + recipesHasIngredientsListOldRecipesHasIngredients + " since its measurementUnits field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<RecipesHasIngredients> attachedRecipesHasIngredientsListNew = new ArrayList<RecipesHasIngredients>();
            for (RecipesHasIngredients recipesHasIngredientsListNewRecipesHasIngredientsToAttach : recipesHasIngredientsListNew) {
                recipesHasIngredientsListNewRecipesHasIngredientsToAttach = em.getReference(recipesHasIngredientsListNewRecipesHasIngredientsToAttach.getClass(), recipesHasIngredientsListNewRecipesHasIngredientsToAttach.getRecipesHasIngredientsPK());
                attachedRecipesHasIngredientsListNew.add(recipesHasIngredientsListNewRecipesHasIngredientsToAttach);
            }
            recipesHasIngredientsListNew = attachedRecipesHasIngredientsListNew;
            measurementUnits.setRecipesHasIngredientsList(recipesHasIngredientsListNew);
            measurementUnits = em.merge(measurementUnits);
            for (RecipesHasIngredients recipesHasIngredientsListNewRecipesHasIngredients : recipesHasIngredientsListNew) {
                if (!recipesHasIngredientsListOld.contains(recipesHasIngredientsListNewRecipesHasIngredients)) {
                    MeasurementUnits oldMeasurementUnitsOfRecipesHasIngredientsListNewRecipesHasIngredients = recipesHasIngredientsListNewRecipesHasIngredients.getMeasurementUnits();
                    recipesHasIngredientsListNewRecipesHasIngredients.setMeasurementUnits(measurementUnits);
                    recipesHasIngredientsListNewRecipesHasIngredients = em.merge(recipesHasIngredientsListNewRecipesHasIngredients);
                    if (oldMeasurementUnitsOfRecipesHasIngredientsListNewRecipesHasIngredients != null && !oldMeasurementUnitsOfRecipesHasIngredientsListNewRecipesHasIngredients.equals(measurementUnits)) {
                        oldMeasurementUnitsOfRecipesHasIngredientsListNewRecipesHasIngredients.getRecipesHasIngredientsList().remove(recipesHasIngredientsListNewRecipesHasIngredients);
                        oldMeasurementUnitsOfRecipesHasIngredientsListNewRecipesHasIngredients = em.merge(oldMeasurementUnitsOfRecipesHasIngredientsListNewRecipesHasIngredients);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = measurementUnits.getId();
                if (findMeasurementUnits(id) == null) {
                    throw new NonexistentEntityException("The measurementUnits with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MeasurementUnits measurementUnits;
            try {
                measurementUnits = em.getReference(MeasurementUnits.class, id);
                measurementUnits.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The measurementUnits with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<RecipesHasIngredients> recipesHasIngredientsListOrphanCheck = measurementUnits.getRecipesHasIngredientsList();
            for (RecipesHasIngredients recipesHasIngredientsListOrphanCheckRecipesHasIngredients : recipesHasIngredientsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This MeasurementUnits (" + measurementUnits + ") cannot be destroyed since the RecipesHasIngredients " + recipesHasIngredientsListOrphanCheckRecipesHasIngredients + " in its recipesHasIngredientsList field has a non-nullable measurementUnits field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(measurementUnits);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MeasurementUnits> findMeasurementUnitsEntities() {
        return findMeasurementUnitsEntities(true, -1, -1);
    }

    public List<MeasurementUnits> findMeasurementUnitsEntities(int maxResults, int firstResult) {
        return findMeasurementUnitsEntities(false, maxResults, firstResult);
    }

    private List<MeasurementUnits> findMeasurementUnitsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MeasurementUnits.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MeasurementUnits findMeasurementUnits(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MeasurementUnits.class, id);
        } finally {
            em.close();
        }
    }

    public int getMeasurementUnitsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MeasurementUnits> rt = cq.from(MeasurementUnits.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

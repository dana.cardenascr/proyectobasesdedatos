/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.exceptions.IllegalOrphanException;
import modelo.exceptions.NonexistentEntityException;

/**
 *
 * @author Ashley
 */
public class RecipesJpaController implements Serializable {

    public RecipesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Recipes recipes) {
        if (recipes.getCategoriesList() == null) {
            recipes.setCategoriesList(new ArrayList<Categories>());
        }
        if (recipes.getRecipesList() == null) {
            recipes.setRecipesList(new ArrayList<Recipes>());
        }
        if (recipes.getRecipesList1() == null) {
            recipes.setRecipesList1(new ArrayList<Recipes>());
        }
        if (recipes.getOccasionsList() == null) {
            recipes.setOccasionsList(new ArrayList<Occasions>());
        }
        if (recipes.getUsersVoteRecipesList() == null) {
            recipes.setUsersVoteRecipesList(new ArrayList<UsersVoteRecipes>());
        }
        if (recipes.getVistisList() == null) {
            recipes.setVistisList(new ArrayList<Vistis>());
        }
        if (recipes.getFeaturedRecipeList() == null) {
            recipes.setFeaturedRecipeList(new ArrayList<FeaturedRecipe>());
        }
        if (recipes.getRecipesHasIngredientsList() == null) {
            recipes.setRecipesHasIngredientsList(new ArrayList<RecipesHasIngredients>());
        }
        if (recipes.getUsersSaveRecipesList() == null) {
            recipes.setUsersSaveRecipesList(new ArrayList<UsersSaveRecipes>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Levels levelsId = recipes.getLevelsId();
            if (levelsId != null) {
                levelsId = em.getReference(levelsId.getClass(), levelsId.getId());
                recipes.setLevelsId(levelsId);
            }
            List<Categories> attachedCategoriesList = new ArrayList<Categories>();
            for (Categories categoriesListCategoriesToAttach : recipes.getCategoriesList()) {
                categoriesListCategoriesToAttach = em.getReference(categoriesListCategoriesToAttach.getClass(), categoriesListCategoriesToAttach.getId());
                attachedCategoriesList.add(categoriesListCategoriesToAttach);
            }
            recipes.setCategoriesList(attachedCategoriesList);
            List<Recipes> attachedRecipesList = new ArrayList<Recipes>();
            for (Recipes recipesListRecipesToAttach : recipes.getRecipesList()) {
                recipesListRecipesToAttach = em.getReference(recipesListRecipesToAttach.getClass(), recipesListRecipesToAttach.getId());
                attachedRecipesList.add(recipesListRecipesToAttach);
            }
            recipes.setRecipesList(attachedRecipesList);
            List<Recipes> attachedRecipesList1 = new ArrayList<Recipes>();
            for (Recipes recipesList1RecipesToAttach : recipes.getRecipesList1()) {
                recipesList1RecipesToAttach = em.getReference(recipesList1RecipesToAttach.getClass(), recipesList1RecipesToAttach.getId());
                attachedRecipesList1.add(recipesList1RecipesToAttach);
            }
            recipes.setRecipesList1(attachedRecipesList1);
            List<Occasions> attachedOccasionsList = new ArrayList<Occasions>();
            for (Occasions occasionsListOccasionsToAttach : recipes.getOccasionsList()) {
                occasionsListOccasionsToAttach = em.getReference(occasionsListOccasionsToAttach.getClass(), occasionsListOccasionsToAttach.getId());
                attachedOccasionsList.add(occasionsListOccasionsToAttach);
            }
            recipes.setOccasionsList(attachedOccasionsList);
            List<UsersVoteRecipes> attachedUsersVoteRecipesList = new ArrayList<UsersVoteRecipes>();
            for (UsersVoteRecipes usersVoteRecipesListUsersVoteRecipesToAttach : recipes.getUsersVoteRecipesList()) {
                usersVoteRecipesListUsersVoteRecipesToAttach = em.getReference(usersVoteRecipesListUsersVoteRecipesToAttach.getClass(), usersVoteRecipesListUsersVoteRecipesToAttach.getUsersVoteRecipesPK());
                attachedUsersVoteRecipesList.add(usersVoteRecipesListUsersVoteRecipesToAttach);
            }
            recipes.setUsersVoteRecipesList(attachedUsersVoteRecipesList);
            List<Vistis> attachedVistisList = new ArrayList<Vistis>();
            for (Vistis vistisListVistisToAttach : recipes.getVistisList()) {
                vistisListVistisToAttach = em.getReference(vistisListVistisToAttach.getClass(), vistisListVistisToAttach.getId());
                attachedVistisList.add(vistisListVistisToAttach);
            }
            recipes.setVistisList(attachedVistisList);
            List<FeaturedRecipe> attachedFeaturedRecipeList = new ArrayList<FeaturedRecipe>();
            for (FeaturedRecipe featuredRecipeListFeaturedRecipeToAttach : recipes.getFeaturedRecipeList()) {
                featuredRecipeListFeaturedRecipeToAttach = em.getReference(featuredRecipeListFeaturedRecipeToAttach.getClass(), featuredRecipeListFeaturedRecipeToAttach.getFeaturedRecipePK());
                attachedFeaturedRecipeList.add(featuredRecipeListFeaturedRecipeToAttach);
            }
            recipes.setFeaturedRecipeList(attachedFeaturedRecipeList);
            List<RecipesHasIngredients> attachedRecipesHasIngredientsList = new ArrayList<RecipesHasIngredients>();
            for (RecipesHasIngredients recipesHasIngredientsListRecipesHasIngredientsToAttach : recipes.getRecipesHasIngredientsList()) {
                recipesHasIngredientsListRecipesHasIngredientsToAttach = em.getReference(recipesHasIngredientsListRecipesHasIngredientsToAttach.getClass(), recipesHasIngredientsListRecipesHasIngredientsToAttach.getRecipesHasIngredientsPK());
                attachedRecipesHasIngredientsList.add(recipesHasIngredientsListRecipesHasIngredientsToAttach);
            }
            recipes.setRecipesHasIngredientsList(attachedRecipesHasIngredientsList);
            List<UsersSaveRecipes> attachedUsersSaveRecipesList = new ArrayList<UsersSaveRecipes>();
            for (UsersSaveRecipes usersSaveRecipesListUsersSaveRecipesToAttach : recipes.getUsersSaveRecipesList()) {
                usersSaveRecipesListUsersSaveRecipesToAttach = em.getReference(usersSaveRecipesListUsersSaveRecipesToAttach.getClass(), usersSaveRecipesListUsersSaveRecipesToAttach.getUsersSaveRecipesPK());
                attachedUsersSaveRecipesList.add(usersSaveRecipesListUsersSaveRecipesToAttach);
            }
            recipes.setUsersSaveRecipesList(attachedUsersSaveRecipesList);
            em.persist(recipes);
            if (levelsId != null) {
                levelsId.getRecipesList().add(recipes);
                levelsId = em.merge(levelsId);
            }
            for (Categories categoriesListCategories : recipes.getCategoriesList()) {
                categoriesListCategories.getRecipesList().add(recipes);
                categoriesListCategories = em.merge(categoriesListCategories);
            }
            for (Recipes recipesListRecipes : recipes.getRecipesList()) {
                recipesListRecipes.getRecipesList().add(recipes);
                recipesListRecipes = em.merge(recipesListRecipes);
            }
            for (Recipes recipesList1Recipes : recipes.getRecipesList1()) {
                recipesList1Recipes.getRecipesList().add(recipes);
                recipesList1Recipes = em.merge(recipesList1Recipes);
            }
            for (Occasions occasionsListOccasions : recipes.getOccasionsList()) {
                occasionsListOccasions.getRecipesList().add(recipes);
                occasionsListOccasions = em.merge(occasionsListOccasions);
            }
            for (UsersVoteRecipes usersVoteRecipesListUsersVoteRecipes : recipes.getUsersVoteRecipesList()) {
                Recipes oldRecipesOfUsersVoteRecipesListUsersVoteRecipes = usersVoteRecipesListUsersVoteRecipes.getRecipes();
                usersVoteRecipesListUsersVoteRecipes.setRecipes(recipes);
                usersVoteRecipesListUsersVoteRecipes = em.merge(usersVoteRecipesListUsersVoteRecipes);
                if (oldRecipesOfUsersVoteRecipesListUsersVoteRecipes != null) {
                    oldRecipesOfUsersVoteRecipesListUsersVoteRecipes.getUsersVoteRecipesList().remove(usersVoteRecipesListUsersVoteRecipes);
                    oldRecipesOfUsersVoteRecipesListUsersVoteRecipes = em.merge(oldRecipesOfUsersVoteRecipesListUsersVoteRecipes);
                }
            }
            for (Vistis vistisListVistis : recipes.getVistisList()) {
                Recipes oldRecipesIdOfVistisListVistis = vistisListVistis.getRecipesId();
                vistisListVistis.setRecipesId(recipes);
                vistisListVistis = em.merge(vistisListVistis);
                if (oldRecipesIdOfVistisListVistis != null) {
                    oldRecipesIdOfVistisListVistis.getVistisList().remove(vistisListVistis);
                    oldRecipesIdOfVistisListVistis = em.merge(oldRecipesIdOfVistisListVistis);
                }
            }
            for (FeaturedRecipe featuredRecipeListFeaturedRecipe : recipes.getFeaturedRecipeList()) {
                Recipes oldRecipesOfFeaturedRecipeListFeaturedRecipe = featuredRecipeListFeaturedRecipe.getRecipes();
                featuredRecipeListFeaturedRecipe.setRecipes(recipes);
                featuredRecipeListFeaturedRecipe = em.merge(featuredRecipeListFeaturedRecipe);
                if (oldRecipesOfFeaturedRecipeListFeaturedRecipe != null) {
                    oldRecipesOfFeaturedRecipeListFeaturedRecipe.getFeaturedRecipeList().remove(featuredRecipeListFeaturedRecipe);
                    oldRecipesOfFeaturedRecipeListFeaturedRecipe = em.merge(oldRecipesOfFeaturedRecipeListFeaturedRecipe);
                }
            }
            for (RecipesHasIngredients recipesHasIngredientsListRecipesHasIngredients : recipes.getRecipesHasIngredientsList()) {
                Recipes oldRecipesOfRecipesHasIngredientsListRecipesHasIngredients = recipesHasIngredientsListRecipesHasIngredients.getRecipes();
                recipesHasIngredientsListRecipesHasIngredients.setRecipes(recipes);
                recipesHasIngredientsListRecipesHasIngredients = em.merge(recipesHasIngredientsListRecipesHasIngredients);
                if (oldRecipesOfRecipesHasIngredientsListRecipesHasIngredients != null) {
                    oldRecipesOfRecipesHasIngredientsListRecipesHasIngredients.getRecipesHasIngredientsList().remove(recipesHasIngredientsListRecipesHasIngredients);
                    oldRecipesOfRecipesHasIngredientsListRecipesHasIngredients = em.merge(oldRecipesOfRecipesHasIngredientsListRecipesHasIngredients);
                }
            }
            for (UsersSaveRecipes usersSaveRecipesListUsersSaveRecipes : recipes.getUsersSaveRecipesList()) {
                Recipes oldRecipesOfUsersSaveRecipesListUsersSaveRecipes = usersSaveRecipesListUsersSaveRecipes.getRecipes();
                usersSaveRecipesListUsersSaveRecipes.setRecipes(recipes);
                usersSaveRecipesListUsersSaveRecipes = em.merge(usersSaveRecipesListUsersSaveRecipes);
                if (oldRecipesOfUsersSaveRecipesListUsersSaveRecipes != null) {
                    oldRecipesOfUsersSaveRecipesListUsersSaveRecipes.getUsersSaveRecipesList().remove(usersSaveRecipesListUsersSaveRecipes);
                    oldRecipesOfUsersSaveRecipesListUsersSaveRecipes = em.merge(oldRecipesOfUsersSaveRecipesListUsersSaveRecipes);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Recipes recipes) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Recipes persistentRecipes = em.find(Recipes.class, recipes.getId());
            Levels levelsIdOld = persistentRecipes.getLevelsId();
            Levels levelsIdNew = recipes.getLevelsId();
            List<Categories> categoriesListOld = persistentRecipes.getCategoriesList();
            List<Categories> categoriesListNew = recipes.getCategoriesList();
            List<Recipes> recipesListOld = persistentRecipes.getRecipesList();
            List<Recipes> recipesListNew = recipes.getRecipesList();
            List<Recipes> recipesList1Old = persistentRecipes.getRecipesList1();
            List<Recipes> recipesList1New = recipes.getRecipesList1();
            List<Occasions> occasionsListOld = persistentRecipes.getOccasionsList();
            List<Occasions> occasionsListNew = recipes.getOccasionsList();
            List<UsersVoteRecipes> usersVoteRecipesListOld = persistentRecipes.getUsersVoteRecipesList();
            List<UsersVoteRecipes> usersVoteRecipesListNew = recipes.getUsersVoteRecipesList();
            List<Vistis> vistisListOld = persistentRecipes.getVistisList();
            List<Vistis> vistisListNew = recipes.getVistisList();
            List<FeaturedRecipe> featuredRecipeListOld = persistentRecipes.getFeaturedRecipeList();
            List<FeaturedRecipe> featuredRecipeListNew = recipes.getFeaturedRecipeList();
            List<RecipesHasIngredients> recipesHasIngredientsListOld = persistentRecipes.getRecipesHasIngredientsList();
            List<RecipesHasIngredients> recipesHasIngredientsListNew = recipes.getRecipesHasIngredientsList();
            List<UsersSaveRecipes> usersSaveRecipesListOld = persistentRecipes.getUsersSaveRecipesList();
            List<UsersSaveRecipes> usersSaveRecipesListNew = recipes.getUsersSaveRecipesList();
            List<String> illegalOrphanMessages = null;
            for (UsersVoteRecipes usersVoteRecipesListOldUsersVoteRecipes : usersVoteRecipesListOld) {
                if (!usersVoteRecipesListNew.contains(usersVoteRecipesListOldUsersVoteRecipes)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UsersVoteRecipes " + usersVoteRecipesListOldUsersVoteRecipes + " since its recipes field is not nullable.");
                }
            }
            for (FeaturedRecipe featuredRecipeListOldFeaturedRecipe : featuredRecipeListOld) {
                if (!featuredRecipeListNew.contains(featuredRecipeListOldFeaturedRecipe)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain FeaturedRecipe " + featuredRecipeListOldFeaturedRecipe + " since its recipes field is not nullable.");
                }
            }
            for (RecipesHasIngredients recipesHasIngredientsListOldRecipesHasIngredients : recipesHasIngredientsListOld) {
                if (!recipesHasIngredientsListNew.contains(recipesHasIngredientsListOldRecipesHasIngredients)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RecipesHasIngredients " + recipesHasIngredientsListOldRecipesHasIngredients + " since its recipes field is not nullable.");
                }
            }
            for (UsersSaveRecipes usersSaveRecipesListOldUsersSaveRecipes : usersSaveRecipesListOld) {
                if (!usersSaveRecipesListNew.contains(usersSaveRecipesListOldUsersSaveRecipes)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UsersSaveRecipes " + usersSaveRecipesListOldUsersSaveRecipes + " since its recipes field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (levelsIdNew != null) {
                levelsIdNew = em.getReference(levelsIdNew.getClass(), levelsIdNew.getId());
                recipes.setLevelsId(levelsIdNew);
            }
            List<Categories> attachedCategoriesListNew = new ArrayList<Categories>();
            for (Categories categoriesListNewCategoriesToAttach : categoriesListNew) {
                categoriesListNewCategoriesToAttach = em.getReference(categoriesListNewCategoriesToAttach.getClass(), categoriesListNewCategoriesToAttach.getId());
                attachedCategoriesListNew.add(categoriesListNewCategoriesToAttach);
            }
            categoriesListNew = attachedCategoriesListNew;
            recipes.setCategoriesList(categoriesListNew);
            List<Recipes> attachedRecipesListNew = new ArrayList<Recipes>();
            for (Recipes recipesListNewRecipesToAttach : recipesListNew) {
                recipesListNewRecipesToAttach = em.getReference(recipesListNewRecipesToAttach.getClass(), recipesListNewRecipesToAttach.getId());
                attachedRecipesListNew.add(recipesListNewRecipesToAttach);
            }
            recipesListNew = attachedRecipesListNew;
            recipes.setRecipesList(recipesListNew);
            List<Recipes> attachedRecipesList1New = new ArrayList<Recipes>();
            for (Recipes recipesList1NewRecipesToAttach : recipesList1New) {
                recipesList1NewRecipesToAttach = em.getReference(recipesList1NewRecipesToAttach.getClass(), recipesList1NewRecipesToAttach.getId());
                attachedRecipesList1New.add(recipesList1NewRecipesToAttach);
            }
            recipesList1New = attachedRecipesList1New;
            recipes.setRecipesList1(recipesList1New);
            List<Occasions> attachedOccasionsListNew = new ArrayList<Occasions>();
            for (Occasions occasionsListNewOccasionsToAttach : occasionsListNew) {
                occasionsListNewOccasionsToAttach = em.getReference(occasionsListNewOccasionsToAttach.getClass(), occasionsListNewOccasionsToAttach.getId());
                attachedOccasionsListNew.add(occasionsListNewOccasionsToAttach);
            }
            occasionsListNew = attachedOccasionsListNew;
            recipes.setOccasionsList(occasionsListNew);
            List<UsersVoteRecipes> attachedUsersVoteRecipesListNew = new ArrayList<UsersVoteRecipes>();
            for (UsersVoteRecipes usersVoteRecipesListNewUsersVoteRecipesToAttach : usersVoteRecipesListNew) {
                usersVoteRecipesListNewUsersVoteRecipesToAttach = em.getReference(usersVoteRecipesListNewUsersVoteRecipesToAttach.getClass(), usersVoteRecipesListNewUsersVoteRecipesToAttach.getUsersVoteRecipesPK());
                attachedUsersVoteRecipesListNew.add(usersVoteRecipesListNewUsersVoteRecipesToAttach);
            }
            usersVoteRecipesListNew = attachedUsersVoteRecipesListNew;
            recipes.setUsersVoteRecipesList(usersVoteRecipesListNew);
            List<Vistis> attachedVistisListNew = new ArrayList<Vistis>();
            for (Vistis vistisListNewVistisToAttach : vistisListNew) {
                vistisListNewVistisToAttach = em.getReference(vistisListNewVistisToAttach.getClass(), vistisListNewVistisToAttach.getId());
                attachedVistisListNew.add(vistisListNewVistisToAttach);
            }
            vistisListNew = attachedVistisListNew;
            recipes.setVistisList(vistisListNew);
            List<FeaturedRecipe> attachedFeaturedRecipeListNew = new ArrayList<FeaturedRecipe>();
            for (FeaturedRecipe featuredRecipeListNewFeaturedRecipeToAttach : featuredRecipeListNew) {
                featuredRecipeListNewFeaturedRecipeToAttach = em.getReference(featuredRecipeListNewFeaturedRecipeToAttach.getClass(), featuredRecipeListNewFeaturedRecipeToAttach.getFeaturedRecipePK());
                attachedFeaturedRecipeListNew.add(featuredRecipeListNewFeaturedRecipeToAttach);
            }
            featuredRecipeListNew = attachedFeaturedRecipeListNew;
            recipes.setFeaturedRecipeList(featuredRecipeListNew);
            List<RecipesHasIngredients> attachedRecipesHasIngredientsListNew = new ArrayList<RecipesHasIngredients>();
            for (RecipesHasIngredients recipesHasIngredientsListNewRecipesHasIngredientsToAttach : recipesHasIngredientsListNew) {
                recipesHasIngredientsListNewRecipesHasIngredientsToAttach = em.getReference(recipesHasIngredientsListNewRecipesHasIngredientsToAttach.getClass(), recipesHasIngredientsListNewRecipesHasIngredientsToAttach.getRecipesHasIngredientsPK());
                attachedRecipesHasIngredientsListNew.add(recipesHasIngredientsListNewRecipesHasIngredientsToAttach);
            }
            recipesHasIngredientsListNew = attachedRecipesHasIngredientsListNew;
            recipes.setRecipesHasIngredientsList(recipesHasIngredientsListNew);
            List<UsersSaveRecipes> attachedUsersSaveRecipesListNew = new ArrayList<UsersSaveRecipes>();
            for (UsersSaveRecipes usersSaveRecipesListNewUsersSaveRecipesToAttach : usersSaveRecipesListNew) {
                usersSaveRecipesListNewUsersSaveRecipesToAttach = em.getReference(usersSaveRecipesListNewUsersSaveRecipesToAttach.getClass(), usersSaveRecipesListNewUsersSaveRecipesToAttach.getUsersSaveRecipesPK());
                attachedUsersSaveRecipesListNew.add(usersSaveRecipesListNewUsersSaveRecipesToAttach);
            }
            usersSaveRecipesListNew = attachedUsersSaveRecipesListNew;
            recipes.setUsersSaveRecipesList(usersSaveRecipesListNew);
            recipes = em.merge(recipes);
            if (levelsIdOld != null && !levelsIdOld.equals(levelsIdNew)) {
                levelsIdOld.getRecipesList().remove(recipes);
                levelsIdOld = em.merge(levelsIdOld);
            }
            if (levelsIdNew != null && !levelsIdNew.equals(levelsIdOld)) {
                levelsIdNew.getRecipesList().add(recipes);
                levelsIdNew = em.merge(levelsIdNew);
            }
            for (Categories categoriesListOldCategories : categoriesListOld) {
                if (!categoriesListNew.contains(categoriesListOldCategories)) {
                    categoriesListOldCategories.getRecipesList().remove(recipes);
                    categoriesListOldCategories = em.merge(categoriesListOldCategories);
                }
            }
            for (Categories categoriesListNewCategories : categoriesListNew) {
                if (!categoriesListOld.contains(categoriesListNewCategories)) {
                    categoriesListNewCategories.getRecipesList().add(recipes);
                    categoriesListNewCategories = em.merge(categoriesListNewCategories);
                }
            }
            for (Recipes recipesListOldRecipes : recipesListOld) {
                if (!recipesListNew.contains(recipesListOldRecipes)) {
                    recipesListOldRecipes.getRecipesList().remove(recipes);
                    recipesListOldRecipes = em.merge(recipesListOldRecipes);
                }
            }
            for (Recipes recipesListNewRecipes : recipesListNew) {
                if (!recipesListOld.contains(recipesListNewRecipes)) {
                    recipesListNewRecipes.getRecipesList().add(recipes);
                    recipesListNewRecipes = em.merge(recipesListNewRecipes);
                }
            }
            for (Recipes recipesList1OldRecipes : recipesList1Old) {
                if (!recipesList1New.contains(recipesList1OldRecipes)) {
                    recipesList1OldRecipes.getRecipesList().remove(recipes);
                    recipesList1OldRecipes = em.merge(recipesList1OldRecipes);
                }
            }
            for (Recipes recipesList1NewRecipes : recipesList1New) {
                if (!recipesList1Old.contains(recipesList1NewRecipes)) {
                    recipesList1NewRecipes.getRecipesList().add(recipes);
                    recipesList1NewRecipes = em.merge(recipesList1NewRecipes);
                }
            }
            for (Occasions occasionsListOldOccasions : occasionsListOld) {
                if (!occasionsListNew.contains(occasionsListOldOccasions)) {
                    occasionsListOldOccasions.getRecipesList().remove(recipes);
                    occasionsListOldOccasions = em.merge(occasionsListOldOccasions);
                }
            }
            for (Occasions occasionsListNewOccasions : occasionsListNew) {
                if (!occasionsListOld.contains(occasionsListNewOccasions)) {
                    occasionsListNewOccasions.getRecipesList().add(recipes);
                    occasionsListNewOccasions = em.merge(occasionsListNewOccasions);
                }
            }
            for (UsersVoteRecipes usersVoteRecipesListNewUsersVoteRecipes : usersVoteRecipesListNew) {
                if (!usersVoteRecipesListOld.contains(usersVoteRecipesListNewUsersVoteRecipes)) {
                    Recipes oldRecipesOfUsersVoteRecipesListNewUsersVoteRecipes = usersVoteRecipesListNewUsersVoteRecipes.getRecipes();
                    usersVoteRecipesListNewUsersVoteRecipes.setRecipes(recipes);
                    usersVoteRecipesListNewUsersVoteRecipes = em.merge(usersVoteRecipesListNewUsersVoteRecipes);
                    if (oldRecipesOfUsersVoteRecipesListNewUsersVoteRecipes != null && !oldRecipesOfUsersVoteRecipesListNewUsersVoteRecipes.equals(recipes)) {
                        oldRecipesOfUsersVoteRecipesListNewUsersVoteRecipes.getUsersVoteRecipesList().remove(usersVoteRecipesListNewUsersVoteRecipes);
                        oldRecipesOfUsersVoteRecipesListNewUsersVoteRecipes = em.merge(oldRecipesOfUsersVoteRecipesListNewUsersVoteRecipes);
                    }
                }
            }
            for (Vistis vistisListOldVistis : vistisListOld) {
                if (!vistisListNew.contains(vistisListOldVistis)) {
                    vistisListOldVistis.setRecipesId(null);
                    vistisListOldVistis = em.merge(vistisListOldVistis);
                }
            }
            for (Vistis vistisListNewVistis : vistisListNew) {
                if (!vistisListOld.contains(vistisListNewVistis)) {
                    Recipes oldRecipesIdOfVistisListNewVistis = vistisListNewVistis.getRecipesId();
                    vistisListNewVistis.setRecipesId(recipes);
                    vistisListNewVistis = em.merge(vistisListNewVistis);
                    if (oldRecipesIdOfVistisListNewVistis != null && !oldRecipesIdOfVistisListNewVistis.equals(recipes)) {
                        oldRecipesIdOfVistisListNewVistis.getVistisList().remove(vistisListNewVistis);
                        oldRecipesIdOfVistisListNewVistis = em.merge(oldRecipesIdOfVistisListNewVistis);
                    }
                }
            }
            for (FeaturedRecipe featuredRecipeListNewFeaturedRecipe : featuredRecipeListNew) {
                if (!featuredRecipeListOld.contains(featuredRecipeListNewFeaturedRecipe)) {
                    Recipes oldRecipesOfFeaturedRecipeListNewFeaturedRecipe = featuredRecipeListNewFeaturedRecipe.getRecipes();
                    featuredRecipeListNewFeaturedRecipe.setRecipes(recipes);
                    featuredRecipeListNewFeaturedRecipe = em.merge(featuredRecipeListNewFeaturedRecipe);
                    if (oldRecipesOfFeaturedRecipeListNewFeaturedRecipe != null && !oldRecipesOfFeaturedRecipeListNewFeaturedRecipe.equals(recipes)) {
                        oldRecipesOfFeaturedRecipeListNewFeaturedRecipe.getFeaturedRecipeList().remove(featuredRecipeListNewFeaturedRecipe);
                        oldRecipesOfFeaturedRecipeListNewFeaturedRecipe = em.merge(oldRecipesOfFeaturedRecipeListNewFeaturedRecipe);
                    }
                }
            }
            for (RecipesHasIngredients recipesHasIngredientsListNewRecipesHasIngredients : recipesHasIngredientsListNew) {
                if (!recipesHasIngredientsListOld.contains(recipesHasIngredientsListNewRecipesHasIngredients)) {
                    Recipes oldRecipesOfRecipesHasIngredientsListNewRecipesHasIngredients = recipesHasIngredientsListNewRecipesHasIngredients.getRecipes();
                    recipesHasIngredientsListNewRecipesHasIngredients.setRecipes(recipes);
                    recipesHasIngredientsListNewRecipesHasIngredients = em.merge(recipesHasIngredientsListNewRecipesHasIngredients);
                    if (oldRecipesOfRecipesHasIngredientsListNewRecipesHasIngredients != null && !oldRecipesOfRecipesHasIngredientsListNewRecipesHasIngredients.equals(recipes)) {
                        oldRecipesOfRecipesHasIngredientsListNewRecipesHasIngredients.getRecipesHasIngredientsList().remove(recipesHasIngredientsListNewRecipesHasIngredients);
                        oldRecipesOfRecipesHasIngredientsListNewRecipesHasIngredients = em.merge(oldRecipesOfRecipesHasIngredientsListNewRecipesHasIngredients);
                    }
                }
            }
            for (UsersSaveRecipes usersSaveRecipesListNewUsersSaveRecipes : usersSaveRecipesListNew) {
                if (!usersSaveRecipesListOld.contains(usersSaveRecipesListNewUsersSaveRecipes)) {
                    Recipes oldRecipesOfUsersSaveRecipesListNewUsersSaveRecipes = usersSaveRecipesListNewUsersSaveRecipes.getRecipes();
                    usersSaveRecipesListNewUsersSaveRecipes.setRecipes(recipes);
                    usersSaveRecipesListNewUsersSaveRecipes = em.merge(usersSaveRecipesListNewUsersSaveRecipes);
                    if (oldRecipesOfUsersSaveRecipesListNewUsersSaveRecipes != null && !oldRecipesOfUsersSaveRecipesListNewUsersSaveRecipes.equals(recipes)) {
                        oldRecipesOfUsersSaveRecipesListNewUsersSaveRecipes.getUsersSaveRecipesList().remove(usersSaveRecipesListNewUsersSaveRecipes);
                        oldRecipesOfUsersSaveRecipesListNewUsersSaveRecipes = em.merge(oldRecipesOfUsersSaveRecipesListNewUsersSaveRecipes);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = recipes.getId();
                if (findRecipes(id) == null) {
                    throw new NonexistentEntityException("The recipes with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Recipes recipes;
            try {
                recipes = em.getReference(Recipes.class, id);
                recipes.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The recipes with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<UsersVoteRecipes> usersVoteRecipesListOrphanCheck = recipes.getUsersVoteRecipesList();
            for (UsersVoteRecipes usersVoteRecipesListOrphanCheckUsersVoteRecipes : usersVoteRecipesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Recipes (" + recipes + ") cannot be destroyed since the UsersVoteRecipes " + usersVoteRecipesListOrphanCheckUsersVoteRecipes + " in its usersVoteRecipesList field has a non-nullable recipes field.");
            }
            List<FeaturedRecipe> featuredRecipeListOrphanCheck = recipes.getFeaturedRecipeList();
            for (FeaturedRecipe featuredRecipeListOrphanCheckFeaturedRecipe : featuredRecipeListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Recipes (" + recipes + ") cannot be destroyed since the FeaturedRecipe " + featuredRecipeListOrphanCheckFeaturedRecipe + " in its featuredRecipeList field has a non-nullable recipes field.");
            }
            List<RecipesHasIngredients> recipesHasIngredientsListOrphanCheck = recipes.getRecipesHasIngredientsList();
            for (RecipesHasIngredients recipesHasIngredientsListOrphanCheckRecipesHasIngredients : recipesHasIngredientsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Recipes (" + recipes + ") cannot be destroyed since the RecipesHasIngredients " + recipesHasIngredientsListOrphanCheckRecipesHasIngredients + " in its recipesHasIngredientsList field has a non-nullable recipes field.");
            }
            List<UsersSaveRecipes> usersSaveRecipesListOrphanCheck = recipes.getUsersSaveRecipesList();
            for (UsersSaveRecipes usersSaveRecipesListOrphanCheckUsersSaveRecipes : usersSaveRecipesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Recipes (" + recipes + ") cannot be destroyed since the UsersSaveRecipes " + usersSaveRecipesListOrphanCheckUsersSaveRecipes + " in its usersSaveRecipesList field has a non-nullable recipes field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Levels levelsId = recipes.getLevelsId();
            if (levelsId != null) {
                levelsId.getRecipesList().remove(recipes);
                levelsId = em.merge(levelsId);
            }
            List<Categories> categoriesList = recipes.getCategoriesList();
            for (Categories categoriesListCategories : categoriesList) {
                categoriesListCategories.getRecipesList().remove(recipes);
                categoriesListCategories = em.merge(categoriesListCategories);
            }
            List<Recipes> recipesList = recipes.getRecipesList();
            for (Recipes recipesListRecipes : recipesList) {
                recipesListRecipes.getRecipesList().remove(recipes);
                recipesListRecipes = em.merge(recipesListRecipes);
            }
            List<Recipes> recipesList1 = recipes.getRecipesList1();
            for (Recipes recipesList1Recipes : recipesList1) {
                recipesList1Recipes.getRecipesList().remove(recipes);
                recipesList1Recipes = em.merge(recipesList1Recipes);
            }
            List<Occasions> occasionsList = recipes.getOccasionsList();
            for (Occasions occasionsListOccasions : occasionsList) {
                occasionsListOccasions.getRecipesList().remove(recipes);
                occasionsListOccasions = em.merge(occasionsListOccasions);
            }
            List<Vistis> vistisList = recipes.getVistisList();
            for (Vistis vistisListVistis : vistisList) {
                vistisListVistis.setRecipesId(null);
                vistisListVistis = em.merge(vistisListVistis);
            }
            em.remove(recipes);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Recipes> findRecipesEntities() {
        return findRecipesEntities(true, -1, -1);
    }

    public List<Recipes> findRecipesEntities(int maxResults, int firstResult) {
        return findRecipesEntities(false, maxResults, firstResult);
    }

    private List<Recipes> findRecipesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Recipes.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Recipes findRecipes(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Recipes.class, id);
        } finally {
            em.close();
        }
    }

    public int getRecipesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Recipes> rt = cq.from(Recipes.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

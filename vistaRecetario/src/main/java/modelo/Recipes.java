/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Ashley
 */
@Entity
@Table(name = "recipes")
@NamedQueries({
    @NamedQuery(name = "Recipes.findAll", query = "SELECT r FROM Recipes r"),
    @NamedQuery(name = "Recipes.findById", query = "SELECT r FROM Recipes r WHERE r.id = :id"),
    @NamedQuery(name = "Recipes.findByCookingTime", query = "SELECT r FROM Recipes r WHERE r.cookingTime = :cookingTime"),
    @NamedQuery(name = "Recipes.findByDescription", query = "SELECT r FROM Recipes r WHERE r.description = :description"),
    @NamedQuery(name = "Recipes.findByImage", query = "SELECT r FROM Recipes r WHERE r.image = :image"),
    @NamedQuery(name = "Recipes.findByName", query = "SELECT r FROM Recipes r WHERE r.name = :name"),
    @NamedQuery(name = "Recipes.findByPortions", query = "SELECT r FROM Recipes r WHERE r.portions = :portions"),
    @NamedQuery(name = "Recipes.findByPreparationInstructions", query = "SELECT r FROM Recipes r WHERE r.preparationInstructions = :preparationInstructions"),
    @NamedQuery(name = "Recipes.findByPreparationTime", query = "SELECT r FROM Recipes r WHERE r.preparationTime = :preparationTime"),
    @NamedQuery(name = "Recipes.findByTotalTime", query = "SELECT r FROM Recipes r WHERE r.totalTime = :totalTime")})
public class Recipes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cooking_time")
    private Float cookingTime;
    @Column(name = "description")
    private String description;
    @Column(name = "image")
    private String image;
    @Column(name = "name")
    private String name;
    @Column(name = "portions")
    private Integer portions;
    @Column(name = "preparation_instructions")
    private String preparationInstructions;
    @Column(name = "preparation_time")
    private Float preparationTime;
    @Column(name = "total_time")
    private Float totalTime;
    @ManyToMany(mappedBy = "recipesList")
    private List<Categories> categoriesList;
    @JoinTable(name = "recipes_similar_recipes", joinColumns = {
        @JoinColumn(name = "recipes_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "recipes_id1", referencedColumnName = "id")})
    @ManyToMany
    private List<Recipes> recipesList;
    @ManyToMany(mappedBy = "recipesList")
    private List<Recipes> recipesList1;
    @ManyToMany(mappedBy = "recipesList")
    private List<Occasions> occasionsList;
    @JoinColumn(name = "levels_id", referencedColumnName = "id")
    @ManyToOne
    private Levels levelsId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipes")
    private List<UsersVoteRecipes> usersVoteRecipesList;
    @OneToMany(mappedBy = "recipesId")
    private List<Vistis> vistisList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipes")
    private List<FeaturedRecipe> featuredRecipeList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipes")
    private List<RecipesHasIngredients> recipesHasIngredientsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipes")
    private List<UsersSaveRecipes> usersSaveRecipesList;
    
    //Comstante
    public static final String[] ETIQUETAS_RECIPES = {"ID", "Name", "Description", "Instrucciones", "Cooking Time", "Preparation Time", "Total Time"};
    public static final int SIZE_RECIPES = ETIQUETAS_RECIPES.length;

    public Recipes() {
    }

    public Recipes(Integer id, Float cookingTime, String description, String image, String name, Integer portions, String preparationInstructions, Float preparationTime, Float totalTime) {
        this.id = id;
        this.cookingTime = cookingTime;
        this.description = description;
        this.image = image;
        this.name = name;
        this.portions = portions;
        this.preparationInstructions = preparationInstructions;
        this.preparationTime = preparationTime;
        this.totalTime = totalTime;
    }

    public Recipes(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(Float cookingTime) {
        this.cookingTime = cookingTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPortions() {
        return portions;
    }

    public void setPortions(Integer portions) {
        this.portions = portions;
    }

    public String getPreparationInstructions() {
        return preparationInstructions;
    }

    public void setPreparationInstructions(String preparationInstructions) {
        this.preparationInstructions = preparationInstructions;
    }

    public Float getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Float preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Float getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Float totalTime) {
        this.totalTime = totalTime;
    }

    public List<Categories> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<Categories> categoriesList) {
        this.categoriesList = categoriesList;
    }

    public List<Recipes> getRecipesList() {
        return recipesList;
    }

    public void setRecipesList(List<Recipes> recipesList) {
        this.recipesList = recipesList;
    }

    public List<Recipes> getRecipesList1() {
        return recipesList1;
    }

    public void setRecipesList1(List<Recipes> recipesList1) {
        this.recipesList1 = recipesList1;
    }

    public List<Occasions> getOccasionsList() {
        return occasionsList;
    }

    public void setOccasionsList(List<Occasions> occasionsList) {
        this.occasionsList = occasionsList;
    }

    public Levels getLevelsId() {
        return levelsId;
    }

    public void setLevelsId(Levels levelsId) {
        this.levelsId = levelsId;
    }

    public List<UsersVoteRecipes> getUsersVoteRecipesList() {
        return usersVoteRecipesList;
    }

    public void setUsersVoteRecipesList(List<UsersVoteRecipes> usersVoteRecipesList) {
        this.usersVoteRecipesList = usersVoteRecipesList;
    }

    public List<Vistis> getVistisList() {
        return vistisList;
    }

    public void setVistisList(List<Vistis> vistisList) {
        this.vistisList = vistisList;
    }

    public List<FeaturedRecipe> getFeaturedRecipeList() {
        return featuredRecipeList;
    }

    public void setFeaturedRecipeList(List<FeaturedRecipe> featuredRecipeList) {
        this.featuredRecipeList = featuredRecipeList;
    }

    public List<RecipesHasIngredients> getRecipesHasIngredientsList() {
        return recipesHasIngredientsList;
    }

    public void setRecipesHasIngredientsList(List<RecipesHasIngredients> recipesHasIngredientsList) {
        this.recipesHasIngredientsList = recipesHasIngredientsList;
    }

    public List<UsersSaveRecipes> getUsersSaveRecipesList() {
        return usersSaveRecipesList;
    }

    public void setUsersSaveRecipesList(List<UsersSaveRecipes> usersSaveRecipesList) {
        this.usersSaveRecipesList = usersSaveRecipesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recipes)) {
            return false;
        }
        Recipes other = (Recipes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    } 

    @Override
    public String toString() {
        return "Recipes{" + "id=" + id + ", cookingTime=" + cookingTime + ", description=" + description + ", image=" + image + ", name=" + name + ", portions=" + portions + ", preparationInstructions=" + preparationInstructions + ", preparationTime=" + preparationTime + ", totalTime=" + totalTime + ", categoriesList=" + categoriesList + ", recipesList=" + recipesList + ", recipesList1=" + recipesList1 + ", occasionsList=" + occasionsList + ", levelsId=" + levelsId + ", usersVoteRecipesList=" + usersVoteRecipesList + ", vistisList=" + vistisList + ", featuredRecipeList=" + featuredRecipeList + ", recipesHasIngredientsList=" + recipesHasIngredientsList + ", usersSaveRecipesList=" + usersSaveRecipesList + '}';
    }

    public String getDatosCelda(int posicion) {
        switch (posicion) {
            case 0:
                return this.getId() + "";
            case 1:
                return this.getName();
            case 2:
                return this.getDescription();
            case 3:
                return this.getPreparationInstructions();
            case 4:
                return this.getCookingTime() + "";
            case 5:
                return this.getPreparationTime()+ "";
            case 6:
                return this.getTotalTime()+ "";

        }
        return null;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Dana
 */
public class ArrayRecipes {

    private ArrayList<Recipes> arrayRecipes;
    private JSONObject baseJSONReceta;
    private File archivo;

    public ArrayRecipes() {
        arrayRecipes = new ArrayList<Recipes>();//ArrayList<>()
        archivo = new File("recipes.json");
        if (archivo.exists()) {
            leerJSON();
        }//Fin if
    }//Fin constructor 

    public int getLongitudArray() {
        return arrayRecipes.size();
    }//Fin getLongitudArray

    public void escribirJSON() {
        JSONArray arregloReceta = new JSONArray();
        baseJSONReceta = new JSONObject();
        for (Recipes recipe : arrayRecipes) {
            JSONObject objJSONReceta = new JSONObject();
            objJSONReceta.put("id", recipe.getId());
            objJSONReceta.put("cooking_time", recipe.getCookingTime());
            objJSONReceta.put("description", recipe.getDescription());
            objJSONReceta.put("image", recipe.getImage());
            objJSONReceta.put("name", recipe.getName());
            objJSONReceta.put("portions", recipe.getPortions());
            objJSONReceta.put("preparationInstructions", recipe.getPreparationInstructions());
            objJSONReceta.put("preparationTime", recipe.getPreparationTime());
            objJSONReceta.put("totalTime", recipe.getTotalTime());
            arregloReceta.add(objJSONReceta);
        }//Fin for
        baseJSONReceta.put("listRecipes", arregloReceta);
        try {
            FileWriter escribir = new FileWriter(archivo);
            escribir.write(baseJSONReceta.toJSONString());
            escribir.flush();
            escribir.close();
        } catch (IOException ex) {
            System.out.println("Hubo un error a la hora de escribir el archivo");
        }//Fin catch
    }//Fin escribir

    public void leerJSON() {
        JSONParser parseador = new JSONParser();
        try {
            FileReader leer = new FileReader(archivo);
            Object obj = parseador.parse(leer);
            baseJSONReceta = (JSONObject) obj;
            JSONArray arregloRecipes = (JSONArray) baseJSONReceta.get("listRecipes");
            for (Object object : arregloRecipes) {
                JSONObject objJSONRecipe = (JSONObject) object;
                Recipes recipe = new Recipes(Integer.parseInt(objJSONRecipe.get("id").toString()), Float.parseFloat(objJSONRecipe.get("cooking_time").toString()), objJSONRecipe.get("description").toString(), objJSONRecipe.get("image").toString(), objJSONRecipe.get("name").toString(), Integer.parseInt(objJSONRecipe.get("portions").toString()), objJSONRecipe.get("preparationInstructions").toString(), Float.parseFloat(objJSONRecipe.get("preparationTime").toString()), Float.parseFloat(objJSONRecipe.get("totalTime").toString()));
                arrayRecipes.add(recipe);
            }//Fin for
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParseException ex) {
            System.out.println("Hubo un error al leer el archivo");
        }//Fin catch
    }//Fin leerJSON

    public String agregar(Recipes recipes) {
        for (int indice = 0; indice < arrayRecipes.size(); indice++) {
            if ((arrayRecipes.get(indice)).getId() == recipes.getId()) {
                return "The recipe had already been added";
            }//Fin if
        }//Fin for
        if (arrayRecipes.add(recipes)) {
            escribirJSON();
            return "The recipe has been successfully registered";
        }//Fin if
        else {
            return "There was an error while registering the recipe";
        }//Fin else
    }//Fin agregar

    public String modificar(Recipes recipe) {
        if (buscar(recipe.getId()) != null) {
            Recipes recipeOriginal = buscar(recipe.getId());
            recipeOriginal.setName(recipe.getName());
            recipeOriginal.setCookingTime(recipe.getCookingTime());
            recipeOriginal.setDescription(recipe.getDescription());
            recipeOriginal.setImage(recipe.getImage());
            recipeOriginal.setPortions(recipe.getPortions());
            recipeOriginal.setPreparationInstructions(recipe.getPreparationInstructions());
            recipeOriginal.setPreparationTime(recipe.getPreparationTime());
            recipeOriginal.setTotalTime(recipe.getTotalTime());
            escribirJSON();
            return "The recipe was successfully modified";
        }//Fin if
        return "This recipe has not been registered";
    }//Fin del modificar

    public String eliminar(int id) {
        if (buscar(id) != null) {
            Recipes recipes = buscar(id);
            if (arrayRecipes.remove(recipes)) {
                escribirJSON();
                return "Recipe successfully deleted";
            }//Fin if
            else {
                return "There was an error when removing the recipe";
            }//Fin else
        } else {
            return "This recipe has not been registered";
        }//Fin else
    }//Fin eliminar

    public Recipes buscar(int id) {
        for (int indice = 0; indice < arrayRecipes.size(); indice++) {
            if ((arrayRecipes.get(indice)).getId() == id) {
                return arrayRecipes.get(indice);
            }//Fin if
        }//Fin for 
        return null;
    }//Fin buscar

    public String[][] reporteTabla() {
        String[][] matrizTabla = new String[this.arrayRecipes.size()][Recipes.SIZE_RECIPES];
        for (int fila = 0; fila < matrizTabla.length; fila++) {
            for (int columna = 0; columna < matrizTabla[fila].length; columna++) {
                matrizTabla[fila][columna] = this.arrayRecipes.get(fila).getDatosCelda(columna);
            }//Fin for columna
        }//Fin for fila
        return matrizTabla;
    }//Fin reporteTabla

    public String toString() {
        String datos = "The registered recipes are: \n";
        for (int indice = 0; indice < arrayRecipes.size(); indice++) {
            datos += arrayRecipes.get(indice).toString() + "\n";
        }//Fin for
        return datos;
    }//Fin toString

    public Recipes getReceta(int numero) {
        for (int indice = 0; indice < arrayRecipes.size(); indice++) {
            if (indice == numero) {
                return arrayRecipes.get(indice);
            }//Fin if
        }//Fin for 
        return null;
    }//Fin getReceta
    
}//Fin clase

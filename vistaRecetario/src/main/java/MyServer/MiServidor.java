/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MyServer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import modelo.Recipes;
import modelo.VistaRecipesWithLevels;

/**
 *
 * @author kmore
 */
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MiServidor implements HttpHandler {

    public static void main(String[] args) {
        int puerto = 8000; 
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(puerto), 0);
            server.createContext("/api", new MiServidor());
            server.start();
            System.out.println("Se ha iniciado la conexión en el puerto " + puerto);
        } catch (IOException e) {
            e.printStackTrace();
        }//Fin catch
    }//Fin try

    @Override
    public void handle(HttpExchange t) throws IOException {
        if (t.getRequestMethod().equalsIgnoreCase("GET")) {
            handleGetViewIngredient(t);
        } else {
            t.sendResponseHeaders(405, -1);
            t.close();
        }//Fin else
    }//Fin handle

    private void handleGetViewIngredient(HttpExchange exchange) throws IOException {
        Gson gson = new Gson();
        Type type = new TypeToken<List<VistaRecipesWithLevels>>() {}.getType();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenciaBD");
        EntityManager em = emf.createEntityManager();
        List<VistaRecipesWithLevels> listView = em.createQuery("SELECT v FROM VistaRecipesWithLevels v", VistaRecipesWithLevels.class)
                .getResultList();

        String response = gson.toJson(listView);
        System.out.println(response);
        exchange.getResponseHeaders().set("Content-Type", "application/json");
        exchange.sendResponseHeaders(200, response.getBytes().length);

        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();

        em.close();
        emf.close();
    }//Fin handleGetViewIngredient

    public void guardar() {
        FileWriter writer = null;
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenciaBD");
        EntityManager em = emf.createEntityManager();
        List<VistaRecipesWithLevels> listView = em.createQuery("SELECT v FROM VistaRecipesWithLevels v", VistaRecipesWithLevels.class)
                .getResultList();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            writer = new FileWriter("recipes.json");
            gson.toJson(listView, writer);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }//Fin if
                em.close();
                emf.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }//Fin catch
        }//Fin finally
    }//Fin guardar
}//Fin clase
